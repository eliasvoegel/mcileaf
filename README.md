# McILEAF
Author: Elias Vögel

Modern vehicles are hightech products composed of multiple microcontrollers running safety relevant software. Microcontrollers have become highly complex systems including various instruction flow optimization measures. These restrict the ability to predict the order in which instructions gets executed by the CPU. To optimize the performance of the code and guarantee correct software, knowledge about the pipelining decisions of the executing CPU is inevitable. Although this knowledge is very relevant for embedded software engineers, there is hardly any information provided by microcontroller vendors. The only solution to this problem is reverse engineeringn of the microarchitectural behaviour using the performance monitoring systems integrated into the controllers. In this bachelor thesis we propose a framework for analzying the pipelining behaviour of microcontroller using different microarchitectures.

# Using the framework
## Requirements
The framework is currently only running on windows. 

Software that has to be installed:
* SEGGER JLINK (assumed to be installed in "c:\Program Files (x86)\SEGGER\JLink\JLink.exe, download from https://www.segger.com/downloads/jlink/
* Python 3, for example from windows store: https://www.microsoft.com/store/productid/9NRWMJP3717K?ocid=pdpshare
* SiFive Freedom Studio, download from https://www.sifive.com/software, install into C:\FreedomStudio\ (otherwise change path in main.py)
* AURIX Studio 1.6, download from https://softwaretools.infineon.com/tools?q=aurix, install into C:\Infineon\AURIX-Studio-1.6.0 (otherwise change path in main.py)

### Python setup
Install libraries:
* pip.exe install serial
* pip.exe install pyserial

## Running tests
Write test cases into TaskCaseArm.h, TestCaseAurix.h and TestCaseRisc.h in the same format as the sample tests that are already in those files.
Select the boards to test in the main function of the main.py program (using the boards array).
Run python3.exe main.py in the source folder.
After successfully running the tests, read the results from result_arm.txt, result_aurix.txt and result_risc.txt.
