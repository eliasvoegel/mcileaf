#pragma once

#include "Test.h"
#include "Uart.hpp"

/**
 * UART communication
*/
class Communication
{
private:
    /// @brief  Baud rate in bits/second
    constexpr static uint32_t baud_rate_ = 115200;

    /// @brief UART handle
    Uart uart_{baud_rate_, nullptr, nullptr};

public:
    /// @brief Constructor
    Communication() = default;

    /// @brief Initialize UART
    void init()
    {
        uart_.init();
    }

    /// @brief Send test results to PC
    /// @param test_results Test results to send
    /// @return True if successful, false if not.
    bool sendTestResult(const TestResults& test_results)
    {
        const char* identifier = "arm";
        uart_.transmit(reinterpret_cast<const uint8_t *>(identifier), strlen(identifier));

        uart_.waitTransmission();

        for (const auto& test_result : test_results.results_)
        {
            if(!uart_.transmit((uint8_t*)&test_result, sizeof(test_result)))
            {
                return false;
            }

            uart_.waitTransmission();
        }
        return true;
    }
};