#pragma once

#include "xmc_usic.h"
#include "xmc_uart.h"
#include "xmc4_usic_map.h"
#include "xmc_gpio.h"
#include "xmc4_gpio.h"
#include "xmc4_gpio_map.h"

class Uart
{
private:
    using callback_t = void(*)();

    /// @brief XMC UART configuration structure
    XMC_UART_CH_CONFIG_t config_ = {
            1000U,  // baudrate
            false,  // normal divider mode instead of fractional divider
            8U,  // data bits
            8U,  // frame length
            1U,  // stop bits
            8U,  // oversampling
            XMC_USIC_CH_PARITY_MODE_NONE	// parity mode
    };

    XMC_USIC_CH_t* channel_ptr_ = XMC_USIC1_CH1;

    XMC_GPIO_PORT_t* port_rx_ = reinterpret_cast<XMC_GPIO_PORT_t *>(PORT0_BASE);
    XMC_GPIO_PORT_t* port_tx_ = reinterpret_cast<XMC_GPIO_PORT_t *>(PORT0_BASE);
    XMC_GPIO_CONFIG_t config_rx_{ XMC_GPIO_MODE_INPUT_PULL_UP, XMC_GPIO_OUTPUT_LEVEL_HIGH, XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE };
    XMC_GPIO_CONFIG_t config_tx_{ XMC_GPIO_MODE_OUTPUT_PUSH_PULL_ALT2, XMC_GPIO_OUTPUT_LEVEL_HIGH, XMC_GPIO_OUTPUT_STRENGTH_STRONG_SOFT_EDGE };


    /// @brief Transmission busy/ongoing
    bool tx_busy_;

    /// @brief Receiving busy/ongoing
    bool rx_busy_;

    /// @brief Transmission data pointer
    const uint8_t* tx_data_;

    /// @brief Receive data buffer pointer
    uint8_t* rx_data_;

    /// @brief Amount of bytes to transmit
    uint16_t tx_data_count_;

    /// @brief Amount of bytes to receive
    uint16_t rx_data_count_;

    /// @brief Current transmitted byte index
    uint16_t tx_data_index_;

    /// @brief Current receiving byte index
    uint16_t rx_data_index_;

    /// @brief TX callback function pointer
    callback_t tx_callback_;

    /// @brief RX callbcak function pointer
    callback_t rx_callback_;

public:
    /// @brief Constructor
    /// @param baud_rate Baud rate in bits/second
    /// @param tx_callback TX callback function pointer
    /// @param rx_callback RX callback function pointer
    Uart(uint32_t baud_rate, callback_t tx_callback, callback_t rx_callback);

    /// @brief Initialize UART
    void init();

    /// @brief Transmit data
    /// @param data Data array
    /// @param length Amount of bytes to send
    /// @return True if successful, false if not
    bool transmit(const uint8_t* data, uint32_t length);

    /// @brief Receive data
    /// @param data_buffer Data buffer to store incoming data
    /// @param length Length of data buffer in number of bytes
    /// @return True if successful, false if not
    bool receive(uint8_t* data_buffer, uint32_t length);

    /// @brief Transmit single byte
    /// @param byte Byte to send
    void transmitByte(uint8_t byte);

    /// @brief Receive single byte
    /// @return Received byte
    uint8_t receiveByte();

    /// @brief Transmission finished handler
    void transmitHandler();

    /// @brief Receive new data handler
    void receiveHandler();

    /// @brief Wait for transmission to finish
    void waitTransmission();
};