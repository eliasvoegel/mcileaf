#include "Uart.hpp"

static Uart* uart = nullptr;

#define Usic1_IrqHandler0		IRQ_Hdlr_90
#define Usic1_IrqHandler1		IRQ_Hdlr_91

extern "C"
{
    ///-----------------------------------------------------------------------------
    void Usic1_IrqHandler0()
    {
        if(uart == nullptr)
        {
            return;
        }

        uart->transmitHandler();
    }

    ///-----------------------------------------------------------------------------
    void Usic1_IrqHandler1()
    {
        if(uart == nullptr)
        {
            return;
        }

        uart->receiveHandler();
    }
}

Uart::Uart(uint32_t baud_rate, callback_t tx_callback, callback_t rx_callback)
    : tx_callback_(tx_callback), rx_callback_(rx_callback)
{
    config_.baudrate = baud_rate;
}

void Uart::init()
{
    // Configure Receive pin
    XMC_GPIO_Init(port_rx_, 0, &config_rx_);

    // Initialize USIC channel in UART mode
    XMC_UART_CH_Init(channel_ptr_, &config_);

    // Set input source path
    XMC_USIC_CH_SetInputSource(channel_ptr_, XMC_USIC_CH_INPUT_DX0, XMC_USIC_CH_INPUT_DX3);

    // Start UART
    XMC_UART_CH_Start(channel_ptr_);

    // Initialize UART TX pin
    XMC_GPIO_Init(port_tx_, 1, &config_tx_);


    // Set priority and enable the IRQ
    NVIC_SetPriority(static_cast<IRQn_Type>(90UL), 50UL);
    NVIC_EnableIRQ(static_cast<IRQn_Type>(90UL));

    // Set the interrupt node pointer to this service request number for TX interrupt
    XMC_USIC_CH_SetInterruptNodePointer(channel_ptr_, XMC_USIC_CH_INTERRUPT_NODE_POINTER_TRANSMIT_BUFFER, 0);


    // Set priority and enable the IRQ
    NVIC_SetPriority(static_cast<IRQn_Type>(91UL), 50UL);
    NVIC_EnableIRQ(static_cast<IRQn_Type>(91UL));

    XMC_USIC_CH_SetInterruptNodePointer(channel_ptr_, XMC_USIC_CH_INTERRUPT_NODE_POINTER_RECEIVE, 1);
    XMC_USIC_CH_SetInterruptNodePointer(channel_ptr_, XMC_USIC_CH_INTERRUPT_NODE_POINTER_ALTERNATE_RECEIVE, 1);

    uart = this;
}

bool Uart::transmit(const uint8_t* data, uint32_t length)
{
    if (tx_busy_)
    {
        return false;
    }

    // If there is no transmission in progress
    if ((data != nullptr) && (length > 0U))
    {
        // Obtain the address of data, size of data
        tx_data_ = data;
        tx_data_count_ = length;
        // Initialize to first index and set the busy flag
        tx_data_index_ = 0U;
        tx_busy_ = true;

        XMC_USIC_CH_EnableEvent(channel_ptr_, static_cast<uint32_t>(XMC_USIC_CH_EVENT_TRANSMIT_BUFFER));

        // Trigger the transmit buffer interrupt
        XMC_USIC_CH_TriggerServiceRequest(channel_ptr_, 0UL);
    }
    else
    {
        return false;
    }

    return true;
}

bool Uart::receive(uint8_t* data_buffer, uint32_t length)
{
    if (rx_busy_)
    {
        return false;
    }

    // If no active reception in progress
    if ((data_buffer != nullptr) && (length > 0U))
    {
        // Obtain the address of data buffer and number of data bytes to be received
        rx_data_ = data_buffer;
        rx_data_count_ = length;
        rx_busy_ = true;
        rx_data_index_ = 0U;

        XMC_USIC_CH_EnableEvent(channel_ptr_, static_cast<uint32_t>(static_cast<uint32_t>(XMC_USIC_CH_EVENT_STANDARD_RECEIVE) | static_cast<uint32_t>(XMC_USIC_CH_EVENT_ALTERNATIVE_RECEIVE)));
    }
    else
    {
        return false;
    }

    return true;
}

void Uart::transmitByte(uint8_t byte)
{
    uint16_t data = byte;
    XMC_UART_CH_Transmit(channel_ptr_, data);
}

uint8_t Uart::receiveByte()
{
    return XMC_UART_CH_GetReceivedData(channel_ptr_);
}

void Uart::transmitHandler()
{
    if (tx_data_index_ < tx_data_count_)
    {
        // When Transmit FIFO is disabled
        transmitByte(tx_data_[tx_data_index_]);
        (tx_data_index_)++;
    }
    else
    {
        if (XMC_USIC_CH_TXFIFO_IsEmpty(channel_ptr_) == true)
        {
            // Disable the standard transmit event
            XMC_USIC_CH_DisableEvent(channel_ptr_, static_cast<uint32_t>(XMC_USIC_CH_EVENT_TRANSMIT_BUFFER));

            // Wait for the transmit buffer to be free to ensure that all data is transmitted
            while (XMC_USIC_CH_GetTransmitBufferStatus(channel_ptr_) == XMC_USIC_CH_TBUF_STATUS_BUSY)
            {

            }
            // All data is transmitted
            tx_busy_ = false;
            tx_data_ = nullptr;

            if (tx_callback_ != nullptr)
            {
                // Execute the callback function provided in the UART APP UI
                tx_callback_();
            }
        }
    }
}

void Uart::receiveHandler()
{
    // When RxFIFO is disabled
    if (rx_data_index_ < rx_data_count_)
    {
        rx_data_[rx_data_index_] = receiveByte();
        rx_data_index_++;
    }

    if (rx_data_index_ == rx_data_count_)
    {
        // Reception complete
        rx_busy_ = false;
        // Disable both standard receive and alternative receive FIFO events
        XMC_USIC_CH_DisableEvent(channel_ptr_, static_cast<uint32_t>(static_cast<uint32_t>(XMC_USIC_CH_EVENT_ALTERNATIVE_RECEIVE) | static_cast<uint32_t>(XMC_USIC_CH_EVENT_STANDARD_RECEIVE)));

        if (rx_callback_ != nullptr)
        {
            // Execute the 'End of reception' callback function
            rx_callback_();
        }
    }
}

void Uart::waitTransmission()
{
    while(tx_busy_);
}