#include <cstdint>
#include <xmc_device.h>
#include <xmc_gpio.h>
#include "Test.h"
#include "Communication.hpp"

#include "TestCaseArm.h"

void InitSysTick()
{
    SysTick_Config(1'000'000UL);
    NVIC_DisableIRQ(SysTick_IRQn);
    SysTick->CTRL = SysTick->CTRL & ~SysTick_CTRL_TICKINT_Msk;
}

int main(void)
{
    // Initialize the systick timer used for measuring the cycles of a test cases
    InitSysTick();

    Result_t result;

    // Initialize test context (intial and expected register values) from test case data in TestCaseArm.h
    TestContext_t test1_context = { .reg_initial_ = test1_reg_initial, .reg_expected_ = test1_reg_expected };

    XMC_GPIO_CONFIG_t led0_config {
            .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
            .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW,
            .output_strength = XMC_GPIO_OUTPUT_STRENGTH_MEDIUM
    };

    XMC_GPIO_Init(XMC_GPIO_PORT1, 0, &led0_config);

    // Run test
    static TestResults_t results;
    RUN_TEST(Test1, &test1_context, &results);

    // Analyze the test results (min, max and most common results)
    static TestAnalysis_t analysis;
    analysis = analyzeTestResults(&results);

    // Send test results to PC via UART
    Communication communication{};
    communication.init();
    communication.sendTestResult(results);

	while (true)
    {
        XMC_GPIO_ToggleOutput(XMC_GPIO_PORT1, 0);
        for(uint32_t i = 0; i < 6'000'000UL; i++);
    }
}