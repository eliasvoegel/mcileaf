cmake_minimum_required(VERSION 3.17)

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)

# specify cross compilers and tools
set(CMAKE_C_COMPILER arm-none-eabi-gcc)
set(CMAKE_CXX_COMPILER arm-none-eabi-g++)
set(CMAKE_ASM_COMPILER arm-none-eabi-gcc)
set(CMAKE_AR arm-none-eabi-ar)
set(CMAKE_OBJCOPY arm-none-eabi-objcopy)
set(CMAKE_OBJDUMP arm-none-eabi-objdump)
set(SIZE arm-none-eabi-size)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_C_STANDARD 11)

if(${XMC1400})
    set(MCPU cortex-m0)
else()
    set(MCPU cortex-m4)
endif()


#add_compile_options(--specs=nosys.specs)
add_link_options(--specs=nosys.specs)

# Hardware floating point
if(${XMC1400})
else()
    add_compile_definitions(ARM_MATH_CM4;ARM_MATH_MATRIX_CHECK;ARM_MATH_ROUNDING)
    add_compile_options(-mfloat-abi=hard -mfpu=fpv4-sp-d16)
    add_link_options(-mfloat-abi=hard -mfpu=fpv4-sp-d16)

endif()


add_compile_options(-mcpu=${MCPU} -mthumb -mthumb-interwork)
add_compile_options(-ffunction-sections -fdata-sections -fno-common -fmessage-length=0)

# uncomment to mitigate c++17 absolute addresses warnings
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-register")

if ("${CMAKE_BUILD_TYPE}" STREQUAL "Release")
    message(STATUS "Maximum optimization for speed")
    add_compile_options(-Ofast)
elseif ("${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo")
    message(STATUS "Maximum optimization for speed, debug info included")
    add_compile_options(-Ofast -g)
elseif ("${CMAKE_BUILD_TYPE}" STREQUAL "MinSizeRel")
    message(STATUS "Maximum optimization for size")
    add_compile_options(-Os)
else ()
    message(STATUS "Minimal optimization, debug info included")
    #add_compile_options(-Og -g)
    #add_compile_options(-O0)
endif ()

add_compile_definitions(XMC4500_F100x1024)
set(LINKER_SCRIPT ${CMAKE_SOURCE_DIR}/LinkerScriptXMC4500.lds)


add_link_options(-Wl,-gc-sections,--print-memory-usage,-Map=${PROJECT_BINARY_DIR}/${PROJECT_NAME}.map)
add_link_options(-mcpu=${MCPU} -mthumb -mthumb-interwork)
add_link_options(-T ${LINKER_SCRIPT})

function(after_build_xmc)
    cmake_parse_arguments(
            AFTER_BUILD_PREFIX
            ""
            "TARGET"
            ""
            ${ARGN}
    )

    set(HEX_FILE ${CMAKE_CURRENT_BINARY_DIR}/${AFTER_BUILD_PREFIX_TARGET}.hex)
    set(BIN_FILE ${CMAKE_CURRENT_BINARY_DIR}/${AFTER_BUILD_PREFIX_TARGET}.bin)

    add_custom_command(TARGET ${AFTER_BUILD_PREFIX_TARGET} POST_BUILD
            COMMAND ${CMAKE_OBJCOPY} -Oihex $<TARGET_FILE:${AFTER_BUILD_PREFIX_TARGET}> ${HEX_FILE}
            COMMAND ${CMAKE_OBJCOPY} -Obinary $<TARGET_FILE:${AFTER_BUILD_PREFIX_TARGET}> ${BIN_FILE}
            COMMENT "Building ${HEX_FILE}
Building ${BIN_FILE}")
endfunction()