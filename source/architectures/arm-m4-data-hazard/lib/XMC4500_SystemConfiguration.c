#include "XMC4500_SystemConfiguration.h"
#include <xmc_scu.h>

///-----------------------------------------------------------------------------
void SystemCoreClockSetup(void)
{
    XMC_SCU_HIB_EnableHibernateDomain();

    /* Local data structure for initializing the clock functional block */
    const XMC_SCU_CLOCK_CONFIG_t clock_xmc4_0_config =
            {
                    /* PLL Operating Mode */
                    {
                            80U,										// N divider
                            3U,											// P divider
                            4U,											// K divider
                            XMC_SCU_CLOCK_SYSPLL_MODE_NORMAL,			// PLL Operating Mode
                            XMC_SCU_CLOCK_SYSPLLCLKSRC_OSCHP			// PLL Clock Source
                    },
                    true,				// High Precision Oscillator Operating Mode
                    false,				// Ultra Low Power Oscillator Setting
                    XMC_SCU_CLOCK_FOFI_CALIBRATION_MODE_FACTORY,	// Calibration Mode

                    XMC_SCU_HIB_STDBYCLKSRC_OSI,	// Standby Clock Source
                    XMC_SCU_CLOCK_SYSCLKSRC_PLL,		// System Clock Source
                    1U,					// System Clock Divider Value
                    1U,					// CPU Clock Divider Value
#ifdef MNCP_CLOCK_CCU_ENABLED
                    1U,					// CCU Clock Divider Value
#else
                    0U,					// CCU Clock Divider Value
#endif // MNCP_CLOCK_CCU_ENABLED
                    1U					// Peripheral Clock Divider Value
            };

    /* Initialize the SCU clock */
    XMC_SCU_CLOCK_Init(&clock_xmc4_0_config);
    /* RTC source clock */
    XMC_SCU_HIB_SetRtcClockSource(XMC_SCU_HIB_RTCCLKSRC_OSI);

#ifdef MNCP_CLOCK_USB_ENABLED
    /* USB/SDMMC source clock */
        XMC_SCU_CLOCK_SetUsbClockSource(XMC_SCU_CLOCK_USBCLKSRC_USBPLL);
        /* USB/SDMMC divider setting */
        XMC_SCU_CLOCK_SetUsbClockDivider(4U);
        /* Start USB PLL */
        XMC_SCU_CLOCK_StartUsbPll(1U, 32U);
#endif // MNCP_CLOCK_USB_ENABLED

#ifdef MNCP_CLOCK_WATCHDOG_ENABLED
    /* WDT source clock */
        XMC_SCU_CLOCK_SetWdtClockSource(XMC_SCU_CLOCK_WDTCLKSRC_OFI);
        /* WDT divider setting */
        XMC_SCU_CLOCK_SetWdtClockDivider(1U);
#endif // MNCP_CLOCK_WATCHDOG_ENABLED

#ifdef MNCP_CLOCK_EBU_ENABLED
    /* EBU divider setting */
        XMC_SCU_CLOCK_SetEbuClockDivider(1U);
#endif // MNCP_CLOCK_EBU_ENABLED
}