#include "Uart.h"

void uart_init(Uart_t* uart, uint32_t baud_rate)
{
	uart->config_ = metal_uart_get_device(0);
	metal_uart_init(uart->config_, (int)baud_rate);
}

uint8_t uart_transmit(Uart_t* uart, const uint8_t* data, uint32_t length)
{
	for(uint32_t i = 0; i < length; i++)
	{
		// Wait until ready to send
		while(metal_uart_txready(uart->config_));

		// Send data byte
		if(metal_uart_putc(uart->config_, data[i]))
		{
			return 0;
		}
	}
	return 1;
}

uint8_t uart_receive(Uart_t* uart, uint8_t* data_buffer, uint32_t length)
{
	// Not yet implemented
	return 0;
}
