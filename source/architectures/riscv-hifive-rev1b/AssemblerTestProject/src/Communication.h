/*
 * Communication.h
 *
 *  Created on: 1 Dec 2022
 *      Author: elias
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include <stdint.h>
#include <string.h>
#include "Test.h"
#include "Uart.h"

uint8_t com_send_test_result(Uart_t* uart, const TestResults_t* test_results)
{
    const char* identifier = "risc";
    uart_transmit(uart, (const uint8_t*)identifier, strlen(identifier));

    const uint32_t count = sizeof(TestResults_t)/sizeof(TestResult_t);
    const uint32_t len_result = sizeof(TestResult_t);
    for (size_t i = 0; i < count; i++)
    {
        const TestResult_t* result = &test_results->results_[i];
        if(!uart_transmit(uart, (const uint8_t*)result, len_result))
        {
            return 0x0;
        }
    }
    return 0x1;
}


#endif /* COMMUNICATION_H_ */
