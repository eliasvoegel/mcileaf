//* TEST CASE #6 UNALIGNED ACCESS
#define TEST_VAR_VALUE 0x12345678
#define TEST_VAR_VALUE_SHIFTED 0x56781234
volatile uint32_t test_var0 = TEST_VAR_VALUE;
volatile uint32_t test_var1 = TEST_VAR_VALUE;
volatile uint32_t test_var2 = TEST_VAR_VALUE;

DEFINE_TEST(Test1, 0,
        "LA s1, test_var1\n"
        "ADDI s1, s1, 2\n"
        "LW s2, (s1)\n"
        "ADDI s3, s2, 0\n"
        )

// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = { (uint32_t)&test_var1 + 16, TEST_VAR_VALUE_SHIFTED, TEST_VAR_VALUE_SHIFTED, 0, 0, 0, 0, 0, 0, 0} };
//*/

/* TEST CASE #5 ALIGNED ACCESS
#define TEST_VAR_VALUE 0x12345678
volatile uint32_t test_var1 = TEST_VAR_VALUE;
volatile uint32_t test_var2 = TEST_VAR_VALUE;

DEFINE_TEST(Test1, 0,
        "LA s1, test_var1\n"
        "ADDI s1, s1, 0\n"
        "LW s2, (s1)\n"
        "ADDI s3, s2, 0\n"
        )

// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = { (uint32_t)&test_var1, TEST_VAR_VALUE, TEST_VAR_VALUE, 0, 0, 0, 0, 0, 0, 0} };
//*/


/* TEST CASE #4 NO READ AFTER WRITE HAZARD
DEFINE_TEST(Test1, 0,
        " ADDI s3, zero, 4\n"       // S3 = 4
		" MUL s4, s3, s4\n"         // S4 = S3 * S4 = 4 * 4 = 16
		" ADD s2, s3, s5\n"         // S2 = S3 + S5 = 4 + 16 = 20
        )

// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4,16, 0, 0, 0, 0, 0} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = {1,20, 4,16,16, 0, 0, 0, 0, 0} };
//*/


/* TEST CASE #3 READ AFTER WRITE HAZARD
DEFINE_TEST(Test1, 0,
        " ADDI s3, zero, 4\n"       // S3 = 4
		" MUL s4, s3, s4\n"         // S4 = S3 * S4 = 4 * 4 = 16
		" ADD s2, s3, s4\n"         // S2 = S3 + S4 = 4 + 16 = 20
        )

// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4,16, 0, 0, 0, 0, 0} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = {1,20, 4,16,16, 0, 0, 0, 0, 0} };

//*/

/* TEST CASE #2 REFERENCE INSTRUCTION
DEFINE_TEST(Test1, 0,
            " MUL s1, s2, s3\n"         // S1 = S2 * S3 = 2 * 3 = 6
)

// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = {6, 2, 3, 4, 5, 6, 7, 8, 9, 10} };
//*/

/* TEST CASE #1 EMPTY
DEFINE_TEST(Test1, 0,
)

// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} };
//*/