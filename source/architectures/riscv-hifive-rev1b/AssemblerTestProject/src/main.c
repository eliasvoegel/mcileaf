#include <stdio.h>
#include "Test.h"
#include "Communication.h"
#include <metal/led.h>

#include "TestCaseRisc.h"

Uart_t uart;


int main (void)
{
    Result_t result;

    // Initialize test context (intial and expected register values) from test case data in TestCaseRisc.h
    TestContext_t test1_context = { .reg_initial_ = test1_reg_initial, .reg_expected_ = test1_reg_expected };

    // Run test
    static TestResults_t results;
    RUN_TEST(Test1, &test1_context, &results);

    // Analyze the test results (min, max and most common results)
    static TestAnalysis_t analysis;
    analysis = analyzeTestResults(&results);

    struct metal_led *led0_red, *led0_green, *led0_blue;

    // This demo will toggle LEDs colors so we define them here
    led0_red = metal_led_get_rgb("LD0", "red");
    led0_green = metal_led_get_rgb("LD0", "green");
    led0_blue = metal_led_get_rgb("LD0", "blue");
    if ((led0_red == NULL) || (led0_green == NULL) || (led0_blue == NULL)) {
        printf("At least one of LEDs is null.\n");
        return 1;
    }

    // Enable each LED
    metal_led_enable(led0_red);
    metal_led_enable(led0_green);
    metal_led_enable(led0_blue);

    // Send test results to PC via UART
    uart_init(&uart, 115200);
    com_send_test_result(&uart, &results);

    while(1)
    {

    }

    return 0;
}
