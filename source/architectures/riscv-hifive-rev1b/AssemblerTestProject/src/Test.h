#ifndef TEST_H_
#define TEST_H_

#include <stdint.h>
#include <stdio.h>

/// Tests run for a single test case (each shifted by one NOP instruction)
#define TESTS_PER_TEST_CASE (32UL)

/// Start address of flash
#define MEM_PFLASH_START  0x20000000

/// Offset address of first test
#define FIRST_TEST_OFFSET 0x00020000

/// Offset between each test
#define INTER_TEST_OFFSET 0x00000800

/// Address of first test
#define FIRST_TEST_START (MEM_PFLASH_START + FIRST_TEST_OFFSET)

/// @brief Address of current test
extern size_t __mem_test_location;

/// @brief Current cycle count value
extern volatile uint32_t ccnt;

/// @brief Current instruction count value
extern volatile uint32_t icnt;

/// @brief Current performance counter 3 value
extern volatile uint32_t cnt3;

/// @brief Current performance counter 3 value
extern volatile uint32_t cnt4;

/// @brief Register context (contains registers that are usable for test: S1-S10)
typedef struct RegisterContext
{
    uint32_t data_r_[10];	// S1-S10
} RegisterContext_t;

/// @brief Test context, contains initial, expected and resulting register contexts
typedef struct TestContext
{
	RegisterContext_t reg_initial_;
	RegisterContext_t reg_result_;
	RegisterContext_t reg_expected_;
} TestContext_t;

/// @brief Result type (1 if successful, 0 if not)
typedef unsigned char Result_t;

/// @brief Test result, contains result and cycle/instruction/performance counter 3 and 4 counts
typedef struct TestResult
{
    Result_t result_;
    uint32_t ccnt_;
    uint32_t icnt_;
    uint32_t cnt3_;
    uint32_t cnt4_;
} TestResult_t;

/// @brief Test result for all test runs
typedef struct TestResults
{
    TestResult_t results_[TESTS_PER_TEST_CASE];
} TestResults_t;

/// @brief Analysis result of all test runs (min, max and most common (typical) result for cycle and instruction count)
typedef struct TestAnalysis
{
    uint32_t ccnt_min_;
    uint32_t ccnt_max_;
    uint32_t ccnt_typ_;
    uint32_t icnt_min_;
    uint32_t icnt_max_;
    uint32_t icnt_typ_;
    Result_t result_;
} TestAnalysis_t;

/// @brief Task context of currently running test
static TestContext_t* running_task_context = NULL;

/// @brief Test result of currently running test
static TestResult_t* running_task_result = NULL;

/// Run all test runs for one test case
#define RUN_TEST(TEST_NAME, TEST_CONTEXT, TEST_RESULTS) \
    test_##TEST_NAME##_0(TEST_CONTEXT, &(TEST_RESULTS)->results_[0]); \
    test_##TEST_NAME##_1(TEST_CONTEXT, &(TEST_RESULTS)->results_[1]); \
    test_##TEST_NAME##_2(TEST_CONTEXT, &(TEST_RESULTS)->results_[2]); \
    test_##TEST_NAME##_3(TEST_CONTEXT, &(TEST_RESULTS)->results_[3]); \
    test_##TEST_NAME##_4(TEST_CONTEXT, &(TEST_RESULTS)->results_[4]); \
    test_##TEST_NAME##_5(TEST_CONTEXT, &(TEST_RESULTS)->results_[5]); \
    test_##TEST_NAME##_6(TEST_CONTEXT, &(TEST_RESULTS)->results_[6]); \
    test_##TEST_NAME##_7(TEST_CONTEXT, &(TEST_RESULTS)->results_[7]); \
    test_##TEST_NAME##_8(TEST_CONTEXT, &(TEST_RESULTS)->results_[8]); \
    test_##TEST_NAME##_9(TEST_CONTEXT, &(TEST_RESULTS)->results_[9]); \
    test_##TEST_NAME##_10(TEST_CONTEXT, &(TEST_RESULTS)->results_[10]); \
    test_##TEST_NAME##_11(TEST_CONTEXT, &(TEST_RESULTS)->results_[11]); \
    test_##TEST_NAME##_12(TEST_CONTEXT, &(TEST_RESULTS)->results_[12]); \
    test_##TEST_NAME##_13(TEST_CONTEXT, &(TEST_RESULTS)->results_[13]); \
    test_##TEST_NAME##_14(TEST_CONTEXT, &(TEST_RESULTS)->results_[14]); \
    test_##TEST_NAME##_15(TEST_CONTEXT, &(TEST_RESULTS)->results_[15]); \
    test_##TEST_NAME##_16(TEST_CONTEXT, &(TEST_RESULTS)->results_[16]); \
    test_##TEST_NAME##_17(TEST_CONTEXT, &(TEST_RESULTS)->results_[17]); \
    test_##TEST_NAME##_18(TEST_CONTEXT, &(TEST_RESULTS)->results_[18]); \
    test_##TEST_NAME##_19(TEST_CONTEXT, &(TEST_RESULTS)->results_[19]); \
    test_##TEST_NAME##_20(TEST_CONTEXT, &(TEST_RESULTS)->results_[20]); \
    test_##TEST_NAME##_21(TEST_CONTEXT, &(TEST_RESULTS)->results_[21]); \
    test_##TEST_NAME##_22(TEST_CONTEXT, &(TEST_RESULTS)->results_[22]); \
    test_##TEST_NAME##_23(TEST_CONTEXT, &(TEST_RESULTS)->results_[23]); \
    test_##TEST_NAME##_24(TEST_CONTEXT, &(TEST_RESULTS)->results_[24]); \
    test_##TEST_NAME##_25(TEST_CONTEXT, &(TEST_RESULTS)->results_[25]); \
    test_##TEST_NAME##_26(TEST_CONTEXT, &(TEST_RESULTS)->results_[26]); \
    test_##TEST_NAME##_27(TEST_CONTEXT, &(TEST_RESULTS)->results_[27]); \
    test_##TEST_NAME##_28(TEST_CONTEXT, &(TEST_RESULTS)->results_[28]); \
    test_##TEST_NAME##_29(TEST_CONTEXT, &(TEST_RESULTS)->results_[29]); \
    test_##TEST_NAME##_30(TEST_CONTEXT, &(TEST_RESULTS)->results_[30]); \
    test_##TEST_NAME##_31(TEST_CONTEXT, &(TEST_RESULTS)->results_[31]); \
    test_##TEST_NAME##_32(TEST_CONTEXT, &(TEST_RESULTS)->results_[32]); \
    test_##TEST_NAME##_33(TEST_CONTEXT, &(TEST_RESULTS)->results_[33]); \
    test_##TEST_NAME##_34(TEST_CONTEXT, &(TEST_RESULTS)->results_[34]); \
    test_##TEST_NAME##_35(TEST_CONTEXT, &(TEST_RESULTS)->results_[35]); \
    test_##TEST_NAME##_36(TEST_CONTEXT, &(TEST_RESULTS)->results_[36]); \
    test_##TEST_NAME##_37(TEST_CONTEXT, &(TEST_RESULTS)->results_[37]); \
    test_##TEST_NAME##_38(TEST_CONTEXT, &(TEST_RESULTS)->results_[38]); \
    test_##TEST_NAME##_39(TEST_CONTEXT, &(TEST_RESULTS)->results_[39]); \
    test_##TEST_NAME##_40(TEST_CONTEXT, &(TEST_RESULTS)->results_[40]); \
    test_##TEST_NAME##_41(TEST_CONTEXT, &(TEST_RESULTS)->results_[41]); \
    test_##TEST_NAME##_42(TEST_CONTEXT, &(TEST_RESULTS)->results_[42]); \
    test_##TEST_NAME##_43(TEST_CONTEXT, &(TEST_RESULTS)->results_[43]); \
    test_##TEST_NAME##_44(TEST_CONTEXT, &(TEST_RESULTS)->results_[44]); \
    test_##TEST_NAME##_45(TEST_CONTEXT, &(TEST_RESULTS)->results_[45]); \
    test_##TEST_NAME##_46(TEST_CONTEXT, &(TEST_RESULTS)->results_[46]); \
    test_##TEST_NAME##_47(TEST_CONTEXT, &(TEST_RESULTS)->results_[47]); \
    test_##TEST_NAME##_48(TEST_CONTEXT, &(TEST_RESULTS)->results_[48]); \
    test_##TEST_NAME##_49(TEST_CONTEXT, &(TEST_RESULTS)->results_[49]); \
    test_##TEST_NAME##_50(TEST_CONTEXT, &(TEST_RESULTS)->results_[50]); \
    test_##TEST_NAME##_51(TEST_CONTEXT, &(TEST_RESULTS)->results_[51]); \
    test_##TEST_NAME##_52(TEST_CONTEXT, &(TEST_RESULTS)->results_[52]); \
    test_##TEST_NAME##_53(TEST_CONTEXT, &(TEST_RESULTS)->results_[53]); \
    test_##TEST_NAME##_54(TEST_CONTEXT, &(TEST_RESULTS)->results_[54]); \
    test_##TEST_NAME##_55(TEST_CONTEXT, &(TEST_RESULTS)->results_[55]); \
    test_##TEST_NAME##_56(TEST_CONTEXT, &(TEST_RESULTS)->results_[56]); \
    test_##TEST_NAME##_57(TEST_CONTEXT, &(TEST_RESULTS)->results_[57]); \
    test_##TEST_NAME##_58(TEST_CONTEXT, &(TEST_RESULTS)->results_[58]); \
    test_##TEST_NAME##_59(TEST_CONTEXT, &(TEST_RESULTS)->results_[59]); \
    test_##TEST_NAME##_60(TEST_CONTEXT, &(TEST_RESULTS)->results_[60]); \
    test_##TEST_NAME##_61(TEST_CONTEXT, &(TEST_RESULTS)->results_[61]); \
    test_##TEST_NAME##_62(TEST_CONTEXT, &(TEST_RESULTS)->results_[62]); \
    test_##TEST_NAME##_63(TEST_CONTEXT, &(TEST_RESULTS)->results_[63])

/// Offset NOP instructions for the single test runs, one NOP instruction more for every additional test run
#define I_NOP_0 ""
#define I_NOP_1 " NOP \n"
#define I_NOP_2 I_NOP_1 I_NOP_1
#define I_NOP_3 I_NOP_2 I_NOP_1
#define I_NOP_4 I_NOP_3 I_NOP_1
#define I_NOP_5 I_NOP_4 I_NOP_1
#define I_NOP_6 I_NOP_5 I_NOP_1
#define I_NOP_7 I_NOP_6 I_NOP_1
#define I_NOP_8 I_NOP_7 I_NOP_1
#define I_NOP_9 I_NOP_8 I_NOP_1
#define I_NOP_10 I_NOP_9 I_NOP_1
#define I_NOP_11 I_NOP_10 I_NOP_1
#define I_NOP_12 I_NOP_11 I_NOP_1
#define I_NOP_13 I_NOP_12 I_NOP_1
#define I_NOP_14 I_NOP_13 I_NOP_1
#define I_NOP_15 I_NOP_14 I_NOP_1
#define I_NOP_16 I_NOP_15 I_NOP_1
#define I_NOP_17 I_NOP_16 I_NOP_1
#define I_NOP_18 I_NOP_17 I_NOP_1
#define I_NOP_19 I_NOP_18 I_NOP_1
#define I_NOP_20 I_NOP_19 I_NOP_1
#define I_NOP_21 I_NOP_20 I_NOP_1
#define I_NOP_22 I_NOP_21 I_NOP_1
#define I_NOP_23 I_NOP_22 I_NOP_1
#define I_NOP_24 I_NOP_23 I_NOP_1
#define I_NOP_25 I_NOP_24 I_NOP_1
#define I_NOP_26 I_NOP_25 I_NOP_1
#define I_NOP_27 I_NOP_26 I_NOP_1
#define I_NOP_28 I_NOP_27 I_NOP_1
#define I_NOP_29 I_NOP_28 I_NOP_1
#define I_NOP_30 I_NOP_29 I_NOP_1
#define I_NOP_31 I_NOP_30 I_NOP_1
#define I_NOP_32 I_NOP_31 I_NOP_1
#define I_NOP_33 I_NOP_32 I_NOP_1
#define I_NOP_34 I_NOP_33 I_NOP_1
#define I_NOP_35 I_NOP_34 I_NOP_1
#define I_NOP_36 I_NOP_35 I_NOP_1
#define I_NOP_37 I_NOP_36 I_NOP_1
#define I_NOP_38 I_NOP_37 I_NOP_1
#define I_NOP_39 I_NOP_38 I_NOP_1
#define I_NOP_40 I_NOP_39 I_NOP_1
#define I_NOP_41 I_NOP_40 I_NOP_1
#define I_NOP_42 I_NOP_41 I_NOP_1
#define I_NOP_43 I_NOP_42 I_NOP_1
#define I_NOP_44 I_NOP_43 I_NOP_1
#define I_NOP_45 I_NOP_44 I_NOP_1
#define I_NOP_46 I_NOP_45 I_NOP_1
#define I_NOP_47 I_NOP_46 I_NOP_1
#define I_NOP_48 I_NOP_47 I_NOP_1
#define I_NOP_49 I_NOP_48 I_NOP_1
#define I_NOP_50 I_NOP_49 I_NOP_1
#define I_NOP_51 I_NOP_50 I_NOP_1
#define I_NOP_52 I_NOP_51 I_NOP_1
#define I_NOP_53 I_NOP_52 I_NOP_1
#define I_NOP_54 I_NOP_53 I_NOP_1
#define I_NOP_55 I_NOP_54 I_NOP_1
#define I_NOP_56 I_NOP_55 I_NOP_1
#define I_NOP_57 I_NOP_56 I_NOP_1
#define I_NOP_58 I_NOP_57 I_NOP_1
#define I_NOP_59 I_NOP_58 I_NOP_1
#define I_NOP_60 I_NOP_59 I_NOP_1
#define I_NOP_61 I_NOP_60 I_NOP_1
#define I_NOP_62 I_NOP_61 I_NOP_1
#define I_NOP_63 I_NOP_62 I_NOP_1

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

/// Define single test, put in test section with the passed number of the test run (TEST_NO)
#define __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, TEST_NO) \
__attribute__((section (".tests.test" STR(TEST_NO)))) void test_##TEST_NAME##_##TEST_NO(TestContext_t* test_context, TestResult_t* result) { \
running_task_context = test_context; \
running_task_result = result; \
asm volatile( \
/* Setup initial test case register context */  \
"LUI t0, %%hi(running_task_context)      \n" \
"LW t0, %%lo(running_task_context)(t0)\n" \
"LW s1, 0(t0)   \n" \
"LW s2, 4(t0)   \n" \
"LW s3, 8(t0)   \n" \
"LW s4, 12(t0)  \n" \
"LW s5, 16(t0)  \n" \
"LW s6, 20(t0)  \n" \
"LW s7, 24(t0)  \n" \
"LW s8, 28(t0)  \n" \
"LW s9, 32(t0)  \n" \
"LW s10, 36(t0) \n" \
\
/*  */ \
I_NOP_##TEST_NO \
\
"LI t0, 0x00801\n" \
"CSRW mhpmevent3, t0 \n" \
"LI t0, 0x102 \n" \
"CSRW mhpmevent4, t0 \n" \
"CSRRWI t2, minstret, 0 \n" \
"CSRRWI t1, mhpmcounter4, 0 \n" \
"CSRRWI t0, mhpmcounter3, 0 \n" \
"CSRR t6, mcycle \n" \
\
/* TEST CASE START */ \
TEST_ASM \
/* TEST CASE END   */ \
\
/*  */ \
\
/*  */ \
"CSRR t3, mcycle \n" \
"CSRRWI t0, mhpmcounter3, 0 \n" \
"CSRRWI t1, mhpmcounter4, 0 \n" \
"CSRRWI t2, minstret, 0 \n" \
"ADDI t2, t2, -6 \n" \
"SUB t3, t3, t6 \n" \
"ADDI t3, t3, -1 \n" \
"LUI t4, %%hi(icnt)      \n" \
"SW t2, %%lo(icnt)(t4) \n" \
"LUI t4, %%hi(ccnt)      \n" \
"SW t3, %%lo(ccnt)(t4) \n" \
"LUI t4, %%hi(cnt3)      \n" \
"SW t0, %%lo(cnt3)(t4) \n" \
"LUI t4, %%hi(cnt4)      \n" \
"SW t1, %%lo(cnt4)(t4) \n" \
\
/* Save the resulting register context */ \
"LUI t0, %%hi(running_task_context)      \n" \
"LW t0, %%lo(running_task_context)(t0)\n" \
"SW s1, 40(t0)   \n" \
"SW s2, 44(t0)   \n" \
"SW s3, 48(t0)   \n" \
"SW s4, 52(t0)  \n" \
"SW s5, 56(t0)  \n" \
"SW s6, 60(t0)  \n" \
"SW s7, 64(t0)  \n" \
"SW s8, 68(t0)  \n" \
"SW s9, 72(t0)  \n" \
"SW s10, 76(t0) \n" \
\
::: "s1", "s2", "s3", "s4", "s5", "s6", "s7", "s8", "s9", "s10", "t0", "t1", "t2", "t3", "t4", "t5", "t6");\
running_task_result->icnt_ = icnt; \
running_task_result->ccnt_ = ccnt; \
running_task_result->cnt3_ = cnt3; \
running_task_result->cnt4_ = cnt4; \
checkTestResult(running_task_context, &running_task_result->result_); }

/// Define a test case with all test runs
#define DEFINE_TEST(TEST_NAME, TEST_CASE_NO, TEST_ASM) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 0) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 1) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 2) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 3) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 4) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 5) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 6) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 7) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 8) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 9) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 10) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 11) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 12) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 13) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 14) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 15) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 16) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 17) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 18) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 19) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 20) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 21) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 22) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 23) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 24) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 25) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 26) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 27) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 28) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 29) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 30) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 31) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 32) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 33) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 34) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 35) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 36) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 37) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 38) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 39) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 40) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 41) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 42) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 43) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 44) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 45) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 46) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 47) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 48) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 49) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 50) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 51) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 52) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 53) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 54) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 55) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 56) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 57) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 58) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 59) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 60) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 61) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 62) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 63)


/// @brief Check if test was successful
/// @param test_context Test context of finished test case
/// @param result Set result after check
void checkTestResult(TestContext_t* test_context, Result_t* result);

/// @brief Analyze the test results, calculate min, max and most common result
/// @param results Results of the test case
/// @return Analyzed test result data
TestAnalysis_t analyzeTestResults(TestResults_t* results);


#endif /* TEST_H_ */
