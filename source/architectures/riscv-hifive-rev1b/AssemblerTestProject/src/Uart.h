#ifndef UART_H_
#define UART_H_

#include <stdint.h>
#include "metal/uart.h"

/// @brief UART configuration structure
typedef struct Uart
{
	struct metal_uart* config_;
} Uart_t;

/// @brief Initialize UART
/// @param uart UART configuration
/// @param baud_rate Baud rate in bits/second
void uart_init(Uart_t* uart, uint32_t baud_rate);

/// @brief Transmit via UART
/// @param uart UART configuration
/// @param data Data array to send
/// @param length Length of data in bytes
/// @return 1 if successful, 0 if not
uint8_t uart_transmit(Uart_t* uart, const uint8_t* data, uint32_t length);

/// @brief Receive data via UART
/// @param uart UART configuration
/// @param data_buffer Incoming data buffer
/// @param length Length of buffer in bytes
/// @return 1 if successful, 0 if not
uint8_t uart_receive(Uart_t* uart, uint8_t* data_buffer, uint32_t length);


#endif /* UART_H_ */
