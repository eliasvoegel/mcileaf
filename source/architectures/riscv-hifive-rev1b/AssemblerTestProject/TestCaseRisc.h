DEFINE_TEST(Test1, 0,
        " ADDI s1, zero, 2\n"       // S1 = 2*/
        " ADDI s2, zero, 2\n"       // S2 = 2*/
		" ADD s3, s1, s2\n"         // S3 = S1 + S2 = 4
		" MUL s4, s3, s4\n"         // S4 = S3 * R3 = 2 * 4 = 8*/
        )


// Register initial values for S1-S10
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4, 0, 0, 0, 0, 0, 0} };

// Register expected result values for S1-S10
RegisterContext_t test1_reg_expected = { .data_r_ = {2, 2, 4,16,0, 0, 0, 0, 0, 0} };
