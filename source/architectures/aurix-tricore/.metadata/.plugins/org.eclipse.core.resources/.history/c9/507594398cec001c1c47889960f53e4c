/*
 * Test.h
 *
 * Author: Elias Vogel
 */

#ifndef TEST_H_
#define TEST_H_

#include "Ifx_Types.h"
#include <stdint.h>

#define MEM_PFLASH_START 0x80000000
#define FIRST_TEST_OFFSET 0x00010000H
#define INTER_TEST_OFFSET 0x00000100H
extern size_t __mem_test_location;

extern volatile uint32_t ccnt;
extern volatile uint32_t icnt;

typedef struct RegisterContext
{
        size_t data_r_[10];
        size_t address_r_[10];
} RegisterContext_t;

typedef struct TestContext
{
        RegisterContext_t reg_initial_;
        RegisterContext_t reg_result_;
        RegisterContext_t reg_expected_;
} TestContext_t;

typedef uint8_t Result_t;

typedef struct TestResult
{
    Result_t result_;
    uint32_t ccnt_;
    uint32_t icnt_;
} TestResult_t;

typedef struct TestResults
{
    TestResult_t results_[32];
} TestResults_t;

static TestContext_t* running_task_context = NULL;
static TestResult_t* running_task_result = NULL;

#define RUN_TEST(TEST_NAME, TEST_CONTEXT, TEST_RESULTS) \
    test_##TEST_NAME##_0(TEST_CONTEXT, &(TEST_RESULTS)->results_[0]); \
    test_##TEST_NAME##_1(TEST_CONTEXT, &(TEST_RESULTS)->results_[1]); \
    test_##TEST_NAME##_2(TEST_CONTEXT, &(TEST_RESULTS)->results_[2]); \
    test_##TEST_NAME##_3(TEST_CONTEXT, &(TEST_RESULTS)->results_[3]); \
    test_##TEST_NAME##_4(TEST_CONTEXT, &(TEST_RESULTS)->results_[4]); \
    test_##TEST_NAME##_5(TEST_CONTEXT, &(TEST_RESULTS)->results_[5]); \
    test_##TEST_NAME##_6(TEST_CONTEXT, &(TEST_RESULTS)->results_[6]); \
    test_##TEST_NAME##_7(TEST_CONTEXT, &(TEST_RESULTS)->results_[7]); \
    test_##TEST_NAME##_8(TEST_CONTEXT, &(TEST_RESULTS)->results_[8]); \
    test_##TEST_NAME##_9(TEST_CONTEXT, &(TEST_RESULTS)->results_[9]); \
    test_##TEST_NAME##_10(TEST_CONTEXT, &(TEST_RESULTS)->results_[10]); \
    test_##TEST_NAME##_11(TEST_CONTEXT, &(TEST_RESULTS)->results_[11]); \
    test_##TEST_NAME##_12(TEST_CONTEXT, &(TEST_RESULTS)->results_[12]); \
    test_##TEST_NAME##_13(TEST_CONTEXT, &(TEST_RESULTS)->results_[13]); \
    test_##TEST_NAME##_14(TEST_CONTEXT, &(TEST_RESULTS)->results_[14]); \
    test_##TEST_NAME##_15(TEST_CONTEXT, &(TEST_RESULTS)->results_[15]); \
    test_##TEST_NAME##_16(TEST_CONTEXT, &(TEST_RESULTS)->results_[16]); \
    test_##TEST_NAME##_17(TEST_CONTEXT, &(TEST_RESULTS)->results_[17]); \
    test_##TEST_NAME##_18(TEST_CONTEXT, &(TEST_RESULTS)->results_[18]); \
    test_##TEST_NAME##_19(TEST_CONTEXT, &(TEST_RESULTS)->results_[19]); \
    test_##TEST_NAME##_20(TEST_CONTEXT, &(TEST_RESULTS)->results_[20]); \
    test_##TEST_NAME##_21(TEST_CONTEXT, &(TEST_RESULTS)->results_[21]); \
    test_##TEST_NAME##_22(TEST_CONTEXT, &(TEST_RESULTS)->results_[22]); \
    test_##TEST_NAME##_23(TEST_CONTEXT, &(TEST_RESULTS)->results_[23]); \
    test_##TEST_NAME##_24(TEST_CONTEXT, &(TEST_RESULTS)->results_[24]); \
    test_##TEST_NAME##_25(TEST_CONTEXT, &(TEST_RESULTS)->results_[25]); \
    test_##TEST_NAME##_26(TEST_CONTEXT, &(TEST_RESULTS)->results_[26]); \
    test_##TEST_NAME##_27(TEST_CONTEXT, &(TEST_RESULTS)->results_[27]); \
    test_##TEST_NAME##_28(TEST_CONTEXT, &(TEST_RESULTS)->results_[28]); \
    test_##TEST_NAME##_29(TEST_CONTEXT, &(TEST_RESULTS)->results_[29]); \
    test_##TEST_NAME##_30(TEST_CONTEXT, &(TEST_RESULTS)->results_[30]); \
    test_##TEST_NAME##_31(TEST_CONTEXT, &(TEST_RESULTS)->results_[31])

#define I_NOP_0 ""
#define I_NOP_1 " NOP \n"
#define I_NOP_2 I_NOP_1 I_NOP_1
#define I_NOP_3 I_NOP_2 I_NOP_1
#define I_NOP_4 I_NOP_3 I_NOP_1
#define I_NOP_5 I_NOP_4 I_NOP_1
#define I_NOP_6 I_NOP_5 I_NOP_1
#define I_NOP_7 I_NOP_6 I_NOP_1
#define I_NOP_8 I_NOP_7 I_NOP_1
#define I_NOP_9 I_NOP_8 I_NOP_1
#define I_NOP_10 I_NOP_9 I_NOP_1
#define I_NOP_11 I_NOP_10 I_NOP_1
#define I_NOP_12 I_NOP_11 I_NOP_1
#define I_NOP_13 I_NOP_12 I_NOP_1
#define I_NOP_14 I_NOP_13 I_NOP_1
#define I_NOP_15 I_NOP_14 I_NOP_1
#define I_NOP_16 I_NOP_15 I_NOP_1
#define I_NOP_17 I_NOP_16 I_NOP_1
#define I_NOP_18 I_NOP_17 I_NOP_1
#define I_NOP_19 I_NOP_18 I_NOP_1
#define I_NOP_20 I_NOP_19 I_NOP_1
#define I_NOP_21 I_NOP_20 I_NOP_1
#define I_NOP_22 I_NOP_21 I_NOP_1
#define I_NOP_23 I_NOP_22 I_NOP_1
#define I_NOP_24 I_NOP_23 I_NOP_1
#define I_NOP_25 I_NOP_24 I_NOP_1
#define I_NOP_26 I_NOP_25 I_NOP_1
#define I_NOP_27 I_NOP_26 I_NOP_1
#define I_NOP_28 I_NOP_27 I_NOP_1
#define I_NOP_29 I_NOP_28 I_NOP_1
#define I_NOP_30 I_NOP_29 I_NOP_1
#define I_NOP_31 I_NOP_30 I_NOP_1

#define __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_NO) \
void test_##TEST_NAME##_##TEST_NO(TestContext_t* test_context, TestResult_t* result) { \
running_task_context = test_context; \
running_task_result = result; \
__asm( \
" LEA A13, running_task_context\n" \
" LD.A A13, [A13]\n" \
" LD.W D0, [A13]0 \n" \
" LD.W D1, [A13]4 \n" \
" LD.W D2, [A13]8 \n" \
" LD.W D3, [A13]12 \n" \
" LD.W D4, [A13]16 \n" \
" LD.W D5, [A13]20 \n" \
" LD.W D6, [A13]24 \n" \
" LD.W D7, [A13]28 \n" \
" LD.W D8, [A13]32 \n" \
" LD.W D9, [A13]36 \n" \
" LD.A A0, [A13]40 \n" \
" LD.A A1, [A13]44 \n" \
" LD.A A2, [A13]48 \n" \
" LD.A A3, [A13]52 \n" \
" LD.A A4, [A13]56 \n" \
" LD.A A5, [A13]60 \n" \
" LD.A A6, [A13]64 \n" \
" LD.A A7, [A13]68 \n" \
" LD.A A8, [A13]72 \n" \
" LD.A A9, [A13]76 \n" \
I_NOP_##TEST_NO \
\
" MOV D11,#0\n" \
" MOV D12,#2\n" \
" MTCR #0xFC00,D11\n" \
" MTCR #0xFC04,D11\n" \
" MTCR #0xFC08,D11\n" \
" ISYNC \n" \
" DSYNC \n" \
" MTCR #0xFC00,D12\n" \
\
TEST_ASM \
\
" ISYNC \n" \
" DSYNC \n" \
" MTCR #0xFC00,D11\n" \
" ISYNC \n" \
\
" MFCR D13, #0xFC04\n" \
" MFCR D14, #0xFC08\n" \
\
" MOV D11, #7\n" \
" SUB D13, D13, D11\n" \
" MOV D11, #3\n" \
" SUB D14, D14, D11\n" \
\
" LEA A12, ccnt\n" \
" ST.W [A12], D13\n" \
\
" LEA A12, icnt\n" \
" ST.W [A12], D14\n" \
\
" ST.W [A13]80, D0\n" \
" ST.W [A13]84, D1\n" \
" ST.W [A13]88, D2\n" \
" ST.W [A13]92, D3\n" \
" ST.W [A13]96, D4\n" \
" ST.W [A13]100, D5\n" \
" ST.W [A13]104, D6\n" \
" ST.W [A13]108, D7\n" \
" ST.W [A13]112, D8\n" \
" ST.W [A13]116, D9\n" \
" ST.A [A13]120, A0\n" \
" ST.A [A13]124, A1\n" \
" ST.A [A13]128, A2\n" \
" ST.A [A13]132, A3\n" \
" ST.A [A13]136, A4\n" \
" ST.A [A13]140, A5\n" \
" ST.A [A13]144, A6\n" \
" ST.A [A13]148, A7\n" \
" ST.A [A13]152, A8\n" \
" ST.A [A13]156, A9\n" \
\
:::);\
running_task_result->icnt_ = icnt; \
running_task_result->ccnt_ = ccnt; \
/*checkTestResult(running_task_context, &running_task_result->result_);*/ }

#define DEFINE_TEST(TEST_NAME, TEST_ASM) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 0) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 1) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 2) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 3) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 4) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 5) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 6) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 7) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 8) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 9) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 10) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 11) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 12) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 13) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 14) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 15) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 16) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 17) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 18) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 19) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 20) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 21) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 22) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 23) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 24) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 25) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 26) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 27) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 28) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 29) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 30) \
    __DEFINE_TEST(TEST_NAME, TEST_ASM, 31)


void checkTestResult(TestContext_t* test_context, Result_t* result);

#endif /* TEST_H_ */
