/*
 * Test.h
 *
 * Author: Elias Vogel
 */

#ifndef TEST_H_
#define TEST_H_

#include "Ifx_Types.h"
#include <stdint.h>

#define MEM_PFLASH_START 0x80000000
#define FIRST_TEST_OFFSET 0x00010000H
#define INTER_TEST_OFFSET 0x00000100H
size_t __mem_test_location = MEM_PFLASH_START + FIRST_TEST_OFFSET;

extern volatile uint32_t ccnt;
extern volatile uint32_t icnt;

typedef struct RegisterContext
{
        size_t data_r_[10];
        size_t address_r_[10];
} RegisterContext_t;

typedef struct TestContext
{
        RegisterContext_t reg_initial_;
        RegisterContext_t reg_result_;
        RegisterContext_t reg_expected_;
} TestContext_t;

typedef uint8_t Result_t;

static TestContext_t* running_task_context = NULL;

//#define RUN_TEST(TEST_NAME, TEST_CONTEXT, TEST_RESULT, TEST_ASM) running_task_context = TEST_CONTEXT; __asm( \
        " LEA A13, running_task_context\n" \
        " LD.A A13, [A13]\n" \
        " LD.W D0, [A13]0 \n" \
        " LD.W D1, [A13]4 \n" \
        " LD.W D2, [A13]8 \n" \
        " LD.W D3, [A13]12 \n" \
        " LD.W D4, [A13]16 \n" \
        " LD.W D5, [A13]20 \n" \
        " LD.W D6, [A13]24 \n" \
        " LD.W D7, [A13]28 \n" \
        " LD.W D8, [A13]32 \n" \
        " LD.W D9, [A13]36 \n" \
        " LD.A A0, [A13]40 \n" \
        " LD.A A1, [A13]44 \n" \
        " LD.A A2, [A13]48 \n" \
        " LD.A A3, [A13]52 \n" \
        " LD.A A4, [A13]56 \n" \
        " LD.A A5, [A13]60 \n" \
        " LD.A A6, [A13]64 \n" \
        " LD.A A7, [A13]68 \n" \
        " LD.A A8, [A13]72 \n" \
        " LD.A A9, [A13]76 \n" \
        " NOP \n" \
        " NOP \n" \
        " NOP \n" \
        " NOP \n" \
        \
        " MOV D11,#0\n" \
        " MOV D12,#2\n" \
        " MTCR #0xFC00,D11\n" \
        " MTCR #0xFC04,D11\n" \
        " MTCR #0xFC08,D11\n" \
        " ISYNC \n" \
        " DSYNC \n" \
        " MTCR #0xFC00,D12\n" \
        \
        TEST_ASM \
        \
        " ISYNC \n" \
        " DSYNC \n" \
        " MTCR #0xFC00,D11\n" \
        " ISYNC \n" \
        \
        " MFCR D13, #0xFC04\n" \
        " MFCR D14, #0xFC08\n" \
        \
        " MOV D11, #7\n" \
        " SUB D13, D13, D11\n" \
        " MOV D11, #3\n" \
        " SUB D14, D14, D11\n" \
        \
        " LEA A12, ccnt\n" \
        " ST.W [A12], D13\n" \
        \
        " LEA A12, icnt\n" \
        " ST.W [A12], D14\n" \
        \
        " ST.W [A13]80, D0\n" \
        " ST.W [A13]84, D1\n" \
        " ST.W [A13]88, D2\n" \
        " ST.W [A13]92, D3\n" \
        " ST.W [A13]96, D4\n" \
        " ST.W [A13]100, D5\n" \
        " ST.W [A13]104, D6\n" \
        " ST.W [A13]108, D7\n" \
        " ST.W [A13]112, D8\n" \
        " ST.W [A13]116, D9\n" \
        " ST.A [A13]120, A0\n" \
        " ST.A [A13]124, A1\n" \
        " ST.A [A13]128, A2\n" \
        " ST.A [A13]132, A3\n" \
        " ST.A [A13]136, A4\n" \
        " ST.A [A13]140, A5\n" \
        " ST.A [A13]144, A6\n" \
        " ST.A [A13]148, A7\n" \
        " ST.A [A13]152, A8\n" \
        " ST.A [A13]156, A9\n" \
        \
        :::);\
        checkTestResult(running_task_context, TEST_RESULT)

#define DEFINE_TEST(TEST_NAME, TEST_CONTEXT, TEST_RESULT, TEST_ASM) running_task_context = TEST_CONTEXT; __asm( \
" LEA A13, running_task_context\n" \
" LD.A A13, [A13]\n" \
" LD.W D0, [A13]0 \n" \
" LD.W D1, [A13]4 \n" \
" LD.W D2, [A13]8 \n" \
" LD.W D3, [A13]12 \n" \
" LD.W D4, [A13]16 \n" \
" LD.W D5, [A13]20 \n" \
" LD.W D6, [A13]24 \n" \
" LD.W D7, [A13]28 \n" \
" LD.W D8, [A13]32 \n" \
" LD.W D9, [A13]36 \n" \
" LD.A A0, [A13]40 \n" \
" LD.A A1, [A13]44 \n" \
" LD.A A2, [A13]48 \n" \
" LD.A A3, [A13]52 \n" \
" LD.A A4, [A13]56 \n" \
" LD.A A5, [A13]60 \n" \
" LD.A A6, [A13]64 \n" \
" LD.A A7, [A13]68 \n" \
" LD.A A8, [A13]72 \n" \
" LD.A A9, [A13]76 \n" \
" NOP \n" \
" NOP \n" \
" NOP \n" \
" NOP \n" \
\
" MOV D11,#0\n" \
" MOV D12,#2\n" \
" MTCR #0xFC00,D11\n" \
" MTCR #0xFC04,D11\n" \
" MTCR #0xFC08,D11\n" \
" ISYNC \n" \
" DSYNC \n" \
" MTCR #0xFC00,D12\n" \
\
TEST_ASM \
\
" ISYNC \n" \
" DSYNC \n" \
" MTCR #0xFC00,D11\n" \
" ISYNC \n" \
\
" MFCR D13, #0xFC04\n" \
" MFCR D14, #0xFC08\n" \
\
" MOV D11, #7\n" \
" SUB D13, D13, D11\n" \
" MOV D11, #3\n" \
" SUB D14, D14, D11\n" \
\
" LEA A12, ccnt\n" \
" ST.W [A12], D13\n" \
\
" LEA A12, icnt\n" \
" ST.W [A12], D14\n" \
\
" ST.W [A13]80, D0\n" \
" ST.W [A13]84, D1\n" \
" ST.W [A13]88, D2\n" \
" ST.W [A13]92, D3\n" \
" ST.W [A13]96, D4\n" \
" ST.W [A13]100, D5\n" \
" ST.W [A13]104, D6\n" \
" ST.W [A13]108, D7\n" \
" ST.W [A13]112, D8\n" \
" ST.W [A13]116, D9\n" \
" ST.A [A13]120, A0\n" \
" ST.A [A13]124, A1\n" \
" ST.A [A13]128, A2\n" \
" ST.A [A13]132, A3\n" \
" ST.A [A13]136, A4\n" \
" ST.A [A13]140, A5\n" \
" ST.A [A13]144, A6\n" \
" ST.A [A13]148, A7\n" \
" ST.A [A13]152, A8\n" \
" ST.A [A13]156, A9\n" \
\
:::);\
checkTestResult(running_task_context, TEST_RESULT)

void checkTestResult(TestContext_t* test_context, Result_t* result);

#endif /* TEST_H_ */
