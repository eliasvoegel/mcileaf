/*
 * Test.c
 *
 * Author: Elias Voegel
 */

#include "Test.h"
#include <algorithm>

volatile uint32_t ccnt = 0;
volatile uint32_t icnt = 0;
size_t __mem_test_location = MEM_PFLASH_START + FIRST_TEST_OFFSET;

void checkTestResult(TestContext_t* test_context, Result_t* result)
{
    RegisterContext_t* r_expected = &test_context->reg_expected_;
    RegisterContext_t* r_result = &test_context->reg_result_;

    for(size_t i = 0; i < sizeof(r_expected->data_r_) / sizeof(size_t); i++)
    {
        if(r_expected->data_r_[i] != r_result->data_r_[i])
        {
            *result = 0;
            return;
        }
    }

    for(size_t i = 0; i < sizeof(r_expected->address_r_) / sizeof(size_t); i++)
    {
        if(r_expected->address_r_[i] != r_result->address_r_[i])
        {
            *result = 0;
            return;
        }
    }

    *result = 1;
}

TestAnalysis_t analyzeTestResults(TestResults_t* results)
{
    TestAnalysis_t analysis = { .ccnt_max_ = 0, .ccnt_min_ = UINT_FAST32_MAX, .ccnt_typ_ = 0 };
    uint32_t ccnt_typ_count[TESTS_PER_TEST_CASE][2] = {0};
    uint32_t ccnt_typ_count_max = 0;
    for(uint32_t i = 0; i < TESTS_PER_TEST_CASE; i++)
    {
        uint32_t ccnt = results->results_[i].ccnt_;
        analysis.ccnt_min_ = min(ccnt, analysis.ccnt_min_);
        analysis.ccnt_max_ = max(ccnt, analysis.ccnt_max_);

        for(uint32_t j = 0; j < TESTS_PER_TEST_CASE; j++)
        {
            if(ccnt_typ_count[j][0] == 0)
            {
                ccnt_typ_count[j][0] = ccnt;
                ccnt_typ_count[j][1]++;
                break;
            }

            if(ccnt_typ_count[j][0] == ccnt)
            {
                ccnt_typ_count[j][1]++;
                break;
            }
        }
    }

    for(uint32_t i = 0; i < TESTS_PER_TEST_CASE; i++)
    {
        if(ccnt_typ_count[i][1] > ccnt_typ_count_max)
        {
            ccnt_typ_count_max = ccnt_typ_count[i][1];
            analysis.ccnt_typ_ = ccnt_typ_count[i][0];
        }
    }

    return analysis;
}
