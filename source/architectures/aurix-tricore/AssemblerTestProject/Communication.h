/*
 * Communication.h
 *
 *  Created on: 1 Dec 2022
 *      Author: elias
 */

#ifndef COMMUNICATION_H_
#define COMMUNICATION_H_

#include "Test.h"
#include "Uart.h"
#include "string.h"

uint8_t com_send_test_result(Uart_t* uart, const TestResults_t* test_results)
{
    const char* identifier = "aurix";
    uart_transmit(uart, (const uint8_t*)identifier, strlen(identifier));

    const count = sizeof(TestResults_t)/sizeof(TestResult_t);
    const len_result = sizeof(TestResult_t);
    for (size_t i = 0; i < count; i++)
    {
        const TestResult_t* result = &test_results->results_[i];
        if(!uart_transmit(uart, (const uint8_t*)result, len_result))
        {
            return 0x0;
        }
    }
    return 0x1;
}


#endif /* COMMUNICATION_H_ */
