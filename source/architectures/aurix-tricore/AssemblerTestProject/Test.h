#ifndef TEST_H_
#define TEST_H_

#include "Ifx_Types.h"
#include <stdint.h>

/// Tests run for a single test case (each shifted by one NOP instruction)
#define TESTS_PER_TEST_CASE (32UL)

/// Start address of flash
#define MEM_PFLASH_START 0x80000000

/// Offset address of first test
#define FIRST_TEST_OFFSET 0x00010000

/// Offset between each test
#define INTER_TEST_OFFSET 0x00000800

/// Address of first test
#define FIRST_TEST_START (MEM_PFLASH_START + FIRST_TEST_OFFSET)

/// @brief Address of current test
extern uint32_t __mem_test_location;

/// Selection masks for different M1, M2 and M3 performance counters
#define M1CNT_IP_DISPATCH_STALL (0b000 << 2UL)
#define M1CNT_PCACHE_HIT        (0b001 << 2UL)
#define M1CNT_DCACHE_HIT        (0b010 << 2UL)
#define M1CNT_TOTAL_BRANCH      (0b011 << 2UL)

#define M2CNT_LS_DISPATCH_STALL (0b000 << 5UL)
#define M2CNT_PCACHE_MISS       (0b001 << 5UL)
#define M2CNT_DCACHE_MISS_CLEAN (0b010 << 5UL)
#define M2CNT_PMEM_STALL        (0b011 << 5UL)

#define M3CNT_LP_DISPATCH_STALL (0b000 << 8UL)
#define M3CNT_MULTI_ISSUE       (0b001 << 8UL)
#define M3CNT_DCACHE_MISS_DIRTY (0b010 << 8UL)
#define M3CNT_DMEM_STALL        (0b011 << 8UL)

/// Selected configurations for M1, M2 and M3 performance counters
#define M1CNT_CONFIG M1CNT_PCACHE_HIT
#define M2CNT_CONFIG M2CNT_PCACHE_MISS
#define M3CNT_CONFIG M3CNT_DCACHE_MISS_DIRTY

#define STR(x) #x
#define XSTR(s) STR(s)

// 0x4 | 0x20 | 0x200 = 0x224
#define MXCNT_CONFIG (M1CNT_CONFIG | M2CNT_CONFIG | M3CNT_CONFIG)

/// Enalbe counters mask value
#define CCTRL_COUNTERS_ENABLED 0x226

/// Disable counters mask value
#define CCTRL_COUNTERS_DISABLED 0x224

/// @brief Current cycle count value
extern volatile uint32_t ccnt;

/// @brief Current instruction count value
extern volatile uint32_t icnt;

/// @brief Current performance counter M1 value
extern volatile uint32_t m1cnt;

/// @brief Current performance counter M2 value
extern volatile uint32_t m2cnt;

/// @brief Current performance counter M3 value
extern volatile uint32_t m3cnt;

/// @brief Register context (contains registers that are usable for test: D0-D9 and A0-A9)
typedef struct RegisterContext
{
        uint32_t data_r_[10];
        uint32_t address_r_[10];
} RegisterContext_t;

/// @brief Test context, contains initial, expected and resulting register contexts
typedef struct TestContext
{
        RegisterContext_t reg_initial_;
        RegisterContext_t reg_result_;
        RegisterContext_t reg_expected_;
} TestContext_t;

/// @brief Result type (1 if successful, 0 if not)
typedef uint32_t Result_t;

/// @brief Test result, contains result and cycle/instruction/performance counters counts
typedef struct TestResult
{
    Result_t result_;
    uint32_t ccnt_;
    uint32_t icnt_;
    uint32_t m1cnt_;
    uint32_t m2cnt_;
    uint32_t m3cnt_;
} TestResult_t;

/// @brief Test result for all test runs
typedef struct TestResults
{
    TestResult_t results_[32];
} TestResults_t;

/// @brief Analysis result of all test runs (min, max and most common (typical) result for cycle count)
typedef struct TestAnalysis
{
    uint32_t ccnt_min_;
    uint32_t ccnt_max_;
    uint32_t ccnt_typ_;
    Result_t result_;
} TestAnalysis_t;

/// @brief Task context of currently running test
static TestContext_t* running_task_context = NULL;

/// @brief Test result of currently running test
static TestResult_t* running_task_result = NULL;

/// Run all test runs for one test case
#define RUN_TEST(TEST_NAME, TEST_CONTEXT, TEST_RESULTS) \
    test_##TEST_NAME##_0(TEST_CONTEXT, &(TEST_RESULTS)->results_[0]); \
    test_##TEST_NAME##_1(TEST_CONTEXT, &(TEST_RESULTS)->results_[1]); \
    test_##TEST_NAME##_2(TEST_CONTEXT, &(TEST_RESULTS)->results_[2]); \
    test_##TEST_NAME##_3(TEST_CONTEXT, &(TEST_RESULTS)->results_[3]); \
    test_##TEST_NAME##_4(TEST_CONTEXT, &(TEST_RESULTS)->results_[4]); \
    test_##TEST_NAME##_5(TEST_CONTEXT, &(TEST_RESULTS)->results_[5]); \
    test_##TEST_NAME##_6(TEST_CONTEXT, &(TEST_RESULTS)->results_[6]); \
    test_##TEST_NAME##_7(TEST_CONTEXT, &(TEST_RESULTS)->results_[7]); \
    test_##TEST_NAME##_8(TEST_CONTEXT, &(TEST_RESULTS)->results_[8]); \
    test_##TEST_NAME##_9(TEST_CONTEXT, &(TEST_RESULTS)->results_[9]); \
    test_##TEST_NAME##_10(TEST_CONTEXT, &(TEST_RESULTS)->results_[10]); \
    test_##TEST_NAME##_11(TEST_CONTEXT, &(TEST_RESULTS)->results_[11]); \
    test_##TEST_NAME##_12(TEST_CONTEXT, &(TEST_RESULTS)->results_[12]); \
    test_##TEST_NAME##_13(TEST_CONTEXT, &(TEST_RESULTS)->results_[13]); \
    test_##TEST_NAME##_14(TEST_CONTEXT, &(TEST_RESULTS)->results_[14]); \
    test_##TEST_NAME##_15(TEST_CONTEXT, &(TEST_RESULTS)->results_[15]); \
    test_##TEST_NAME##_16(TEST_CONTEXT, &(TEST_RESULTS)->results_[16]); \
    test_##TEST_NAME##_17(TEST_CONTEXT, &(TEST_RESULTS)->results_[17]); \
    test_##TEST_NAME##_18(TEST_CONTEXT, &(TEST_RESULTS)->results_[18]); \
    test_##TEST_NAME##_19(TEST_CONTEXT, &(TEST_RESULTS)->results_[19]); \
    test_##TEST_NAME##_20(TEST_CONTEXT, &(TEST_RESULTS)->results_[20]); \
    test_##TEST_NAME##_21(TEST_CONTEXT, &(TEST_RESULTS)->results_[21]); \
    test_##TEST_NAME##_22(TEST_CONTEXT, &(TEST_RESULTS)->results_[22]); \
    test_##TEST_NAME##_23(TEST_CONTEXT, &(TEST_RESULTS)->results_[23]); \
    test_##TEST_NAME##_24(TEST_CONTEXT, &(TEST_RESULTS)->results_[24]); \
    test_##TEST_NAME##_25(TEST_CONTEXT, &(TEST_RESULTS)->results_[25]); \
    test_##TEST_NAME##_26(TEST_CONTEXT, &(TEST_RESULTS)->results_[26]); \
    test_##TEST_NAME##_27(TEST_CONTEXT, &(TEST_RESULTS)->results_[27]); \
    test_##TEST_NAME##_28(TEST_CONTEXT, &(TEST_RESULTS)->results_[28]); \
    test_##TEST_NAME##_29(TEST_CONTEXT, &(TEST_RESULTS)->results_[29]); \
    test_##TEST_NAME##_30(TEST_CONTEXT, &(TEST_RESULTS)->results_[30]); \
    test_##TEST_NAME##_31(TEST_CONTEXT, &(TEST_RESULTS)->results_[31])

/// Offset NOP instructions for the single test runs, one NOP instruction more for every additional test run
#define I_NOP_0 ""
#define I_NOP_1 " NOP \n"
#define I_NOP_2 I_NOP_1 I_NOP_1
#define I_NOP_3 I_NOP_2 I_NOP_1
#define I_NOP_4 I_NOP_3 I_NOP_1
#define I_NOP_5 I_NOP_4 I_NOP_1
#define I_NOP_6 I_NOP_5 I_NOP_1
#define I_NOP_7 I_NOP_6 I_NOP_1
#define I_NOP_8 I_NOP_7 I_NOP_1
#define I_NOP_9 I_NOP_8 I_NOP_1
#define I_NOP_10 I_NOP_9 I_NOP_1
#define I_NOP_11 I_NOP_10 I_NOP_1
#define I_NOP_12 I_NOP_11 I_NOP_1
#define I_NOP_13 I_NOP_12 I_NOP_1
#define I_NOP_14 I_NOP_13 I_NOP_1
#define I_NOP_15 I_NOP_14 I_NOP_1
#define I_NOP_16 I_NOP_15 I_NOP_1
#define I_NOP_17 I_NOP_16 I_NOP_1
#define I_NOP_18 I_NOP_17 I_NOP_1
#define I_NOP_19 I_NOP_18 I_NOP_1
#define I_NOP_20 I_NOP_19 I_NOP_1
#define I_NOP_21 I_NOP_20 I_NOP_1
#define I_NOP_22 I_NOP_21 I_NOP_1
#define I_NOP_23 I_NOP_22 I_NOP_1
#define I_NOP_24 I_NOP_23 I_NOP_1
#define I_NOP_25 I_NOP_24 I_NOP_1
#define I_NOP_26 I_NOP_25 I_NOP_1
#define I_NOP_27 I_NOP_26 I_NOP_1
#define I_NOP_28 I_NOP_27 I_NOP_1
#define I_NOP_29 I_NOP_28 I_NOP_1
#define I_NOP_30 I_NOP_29 I_NOP_1
#define I_NOP_31 I_NOP_30 I_NOP_1

/// Define single test, set address according to the passed number of the test run (TEST_NO)
#define __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, TEST_NO) \
void test_##TEST_NAME##_##TEST_NO(TestContext_t* test_context, TestResult_t* result) __at(FIRST_TEST_START + (TESTS_PER_TEST_CASE * TEST_CASE_NO + TEST_NO)* INTER_TEST_OFFSET) { \
running_task_context = test_context; \
running_task_result = result; \
__asm( \
" LEA A13, running_task_context\n" \
" LD.A A13, [A13]\n" \
" LD.W D0, [A13]0 \n" \
" LD.W D1, [A13]4 \n" \
" LD.W D2, [A13]8 \n" \
" LD.W D3, [A13]12 \n" \
" LD.W D4, [A13]16 \n" \
" LD.W D5, [A13]20 \n" \
" LD.W D6, [A13]24 \n" \
" LD.W D7, [A13]28 \n" \
" LD.W D8, [A13]32 \n" \
" LD.W D9, [A13]36 \n" \
" LD.A A0, [A13]40 \n" \
" LD.A A1, [A13]44 \n" \
" LD.A A2, [A13]48 \n" \
" LD.A A3, [A13]52 \n" \
" LD.A A4, [A13]56 \n" \
" LD.A A5, [A13]60 \n" \
" LD.A A6, [A13]64 \n" \
" LD.A A7, [A13]68 \n" \
" LD.A A8, [A13]72 \n" \
" LD.A A9, [A13]76 \n" \
I_NOP_##TEST_NO \
\
/* Clear performance counters (instruction and cycle count) */ \
" MOV D11, #0x0\n" \
" MOV D12, #0x226\n" \
" MTCR #0xFC00,D11\n" \
" MTCR #0xFC04,D11\n" \
" MTCR #0xFC08,D11\n" \
" MTCR #0xFC0C,D11\n" \
" MTCR #0xFC10,D11\n" \
" MTCR #0xFC14,D11\n" \
" ISYNC \n" \
" DSYNC \n" \
\
/* Start performance counters */ \
" MTCR #0xFC00,D12\n" \
\
/* TEST CASE START */ \
TEST_ASM \
/* TEST CASE END   */ \
\
/* Stop performance counters */ \
" ISYNC \n" \
" DSYNC \n" \
" MTCR #0xFC00,D11\n" \
" ISYNC \n" \
\
/* Save values from instruction and cycle count registers, correct values due to preceding and trailing instructions */ \
" MFCR D13, #0xFC04\n" \
" MFCR D14, #0xFC08\n" \
\
" MOV D11, #7\n" \
" SUB D13, D13, D11\n" \
" MOV D11, #3\n" \
" SUB D14, D14, D11\n" \
\
" LEA A12, ccnt\n" \
" ST.W [A12], D13\n" \
\
" LEA A12, icnt\n" \
" ST.W [A12], D14\n" \
\
" MFCR D13, #0xFC0C\n" \
" LEA A12, m1cnt\n" \
" ST.W [A12], D13\n" \
\
" MFCR D13, #0xFC10\n" \
" LEA A12, m2cnt\n" \
" ST.W [A12], D13\n" \
\
" MFCR D13, #0xFC14\n" \
" LEA A12, m3cnt\n" \
" ST.W [A12], D13\n" \
\
/* Save the resulting register context */ \
" ST.W [A13]80, D0\n" \
" ST.W [A13]84, D1\n" \
" ST.W [A13]88, D2\n" \
" ST.W [A13]92, D3\n" \
" ST.W [A13]96, D4\n" \
" ST.W [A13]100, D5\n" \
" ST.W [A13]104, D6\n" \
" ST.W [A13]108, D7\n" \
" ST.W [A13]112, D8\n" \
" ST.W [A13]116, D9\n" \
" ST.A [A13]120, A0\n" \
" ST.A [A13]124, A1\n" \
" ST.A [A13]128, A2\n" \
" ST.A [A13]132, A3\n" \
" ST.A [A13]136, A4\n" \
" ST.A [A13]140, A5\n" \
" ST.A [A13]144, A6\n" \
" ST.A [A13]148, A7\n" \
" ST.A [A13]152, A8\n" \
" ST.A [A13]156, A9\n" \
\
:::);\
running_task_result->icnt_ = icnt; \
running_task_result->ccnt_ = ccnt; \
running_task_result->m1cnt_ = m1cnt; \
running_task_result->m2cnt_ = m2cnt; \
running_task_result->m3cnt_ = m3cnt; \
checkTestResult(running_task_context, &running_task_result->result_); }

/// Define a test case with all test runs
#define DEFINE_TEST(TEST_NAME, TEST_CASE_NO, TEST_ASM) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 0) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 1) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 2) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 3) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 4) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 5) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 6) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 7) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 8) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 9) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 10) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 11) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 12) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 13) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 14) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 15) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 16) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 17) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 18) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 19) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 20) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 21) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 22) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 23) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 24) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 25) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 26) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 27) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 28) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 29) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 30) \
        __DEFINE_TEST(TEST_NAME, TEST_ASM, TEST_CASE_NO, 31)

/// @brief Check if test was successful
/// @param test_context Test context of finished test case
/// @param result Set result after check
void checkTestResult(TestContext_t* test_context, Result_t* result);

/// @brief Analyze the test results, calculate min, max and most common result
/// @param results Results of the test case
/// @return Analyzed test result data
TestAnalysis_t analyzeTestResults(TestResults_t* results);

#endif /* TEST_H_ */
