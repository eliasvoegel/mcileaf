#ifndef UART_H_
#define UART_H_

#include "Ifx_Types.h"
#include <stdint.h>
#include "Libraries/iLLD/TC27D/Tricore/Asclin/Asc/IfxAsclin_Asc.h"
#include "IfxCpu_Irq.h"

#define UART_PIN_RX             IfxAsclin1_RXA_P15_1_IN                 /* UART receive port pin                    */
#define UART_PIN_TX             IfxAsclin1_TX_P15_0_OUT                 /* UART transmit port pin                   */

/* Definition of the interrupt priorities */
#define INTPRIO_ASCLIN1_RX      18
#define INTPRIO_ASCLIN1_TX      19

#define UART_RX_BUFFER_SIZE     64                                      /* Definition of the receive buffer size    */
#define UART_TX_BUFFER_SIZE     64                                      /* Definition of the transmit buffer size   */

/// @brief UART configuration structure
typedef struct Uart
{
    IfxAsclin_Asc_Config config_;
    Ifx_ASCLIN* asclin_;

    /* Declaration of the ASC handle */
    IfxAsclin_Asc asc_handle_;

    /* Declaration of the FIFOs parameters */
    uint8 asc_tx_buffer_[UART_TX_BUFFER_SIZE + sizeof(Ifx_Fifo) + 8];
    uint8 asc_rx_buffer_[UART_RX_BUFFER_SIZE + sizeof(Ifx_Fifo) + 8];
} Uart_t;


/// @brief Initialize UART
/// @param uart UART configuration
/// @param baud_rate Baud rate in bits/second
void uart_init(Uart_t* uart, uint32_t baud_rate);

/// @brief Transmit via UART
/// @param uart UART configuration
/// @param data Data array to send
/// @param length Length of data in bytes
/// @return 1 if successful, 0 if not
uint8_t uart_transmit(Uart_t* uart, const uint8_t* data, uint32_t length);

/// @brief Receive data via UART
/// @param uart UART configuration
/// @param data_buffer Incoming data buffer
/// @param length Length of buffer in bytes
/// @return 1 if successful, 0 if not
uint8_t uart_receive(Uart_t* uart, uint8_t* data_buffer, uint32_t length);


#endif /* UART_H_ */
