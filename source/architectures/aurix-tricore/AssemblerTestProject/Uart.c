#include "Uart.h"

IfxAsclin_Asc* handle = NULL;

IFX_INTERRUPT(asclin1TxISR, 0, INTPRIO_ASCLIN1_TX);
void asclin1TxISR(void)
{
    IfxAsclin_Asc_isrTransmit(handle);
}

IFX_INTERRUPT(asclin1RxISR, 0, INTPRIO_ASCLIN1_RX);
void asclin1RxISR(void)
{
    IfxAsclin_Asc_isrReceive(handle);
}

void uart_init(Uart_t* uart, uint32_t baud_rate)
{
    uart->asclin_ = &MODULE_ASCLIN1;
    IfxAsclin_Asc_initModuleConfig(&uart->config_, uart->asclin_);

    /* Set the desired baud rate */
    uart->config_.baudrate.baudrate = (float32)baud_rate;

    /* ISR priorities and interrupt target */
    uart->config_.interrupt.txPriority = INTPRIO_ASCLIN1_TX;
    uart->config_.interrupt.rxPriority = INTPRIO_ASCLIN1_RX;
    uart->config_.interrupt.typeOfService = IfxCpu_Irq_getTos(IfxCpu_getCoreIndex());

    /* FIFO configuration */
    uart->config_.txBuffer = &uart->asc_tx_buffer_;
    uart->config_.txBufferSize = UART_TX_BUFFER_SIZE;
    uart->config_.rxBuffer = &uart->asc_rx_buffer_;
    uart->config_.rxBufferSize = UART_RX_BUFFER_SIZE;

    /* Pin configuration */
    const IfxAsclin_Asc_Pins pins =
    {
        NULL_PTR,       IfxPort_InputMode_pullUp,     /* CTS pin not used */
        &UART_PIN_RX,   IfxPort_InputMode_pullUp,     /* RX pin           */
        NULL_PTR,       IfxPort_OutputMode_pushPull,  /* RTS pin not used */
        &UART_PIN_TX,   IfxPort_OutputMode_pushPull,  /* TX pin           */
        IfxPort_PadDriver_cmosAutomotiveSpeed1
    };
    uart->config_.pins = &pins;

    IfxAsclin_Asc_initModule(&uart->asc_handle_, &uart->config_); /* Initialize module with above parameters */
    handle = &uart->asc_handle_;

    IfxCpu_enableInterrupts();          /* Enable interrupts after initialization */
}

uint8_t uart_transmit(Uart_t* uart, const uint8_t* data, uint32_t length)
{
    static short int count;
    count = (short int)length;
    return IfxAsclin_Asc_write(&uart->asc_handle_, (void*)data, &count, TIME_INFINITE);
}

uint8_t uart_receive(Uart_t* uart, uint8_t* data_buffer, uint32_t length)
{
    static short int count;
    count = (short int)length;
    return IfxAsclin_Asc_read(&uart->asc_handle_, (void*)data_buffer, &count, TIME_INFINITE);
}
