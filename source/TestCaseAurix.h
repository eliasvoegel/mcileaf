/* TEST CASE #6 UNALIGNED ACCESS
#define TEST_VAR_VALUE 0x1234567812345678
#define TEST_VAR_VALUE_SHIFTED 0x56781234
volatile uint64_t test_var0 = TEST_VAR_VALUE;

DEFINE_TEST(Test1, 0,
        " LEA A0, test_var0\n"
        " LD.W D0, [A0]2 \n"
        " MOV D1, D0\n"
        )

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {TEST_VAR_VALUE_SHIFTED, TEST_VAR_VALUE_SHIFTED, 0, 0, 0, 0, 0, 0, 0, 0}, {(uint32_t)&test_var0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
//*/

//* TEST CASE #5 ALIGNED ACCESS
#define TEST_VAR_VALUE 0x1234567812345678
#define TEST_VAR_VALUE_NOT_SHIFTED 0x12345678
volatile uint64_t test_var0 = TEST_VAR_VALUE;

DEFINE_TEST(Test1, 0,
        " LEA A0, test_var0\n"
        " LD.W D0, [A0]0 \n"
        " MOV D1, D0\n"
        )

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {TEST_VAR_VALUE_NOT_SHIFTED, TEST_VAR_VALUE_NOT_SHIFTED, 0, 0, 0, 0, 0, 0, 0, 0}, {(uint32_t)&test_var0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
//*/

/* TEST CASE #4 NO READ AFTER WRITE HAZARD
DEFINE_TEST(Test1, 0,
            " MOV D2, #4\n"              // D2 = 4
            " MUL D3, D2, D3\n"         // D3 = D2 * D3 = 4 * 4 = 16
            " ADD D1, D2, D4\n"         // D1 = D2 + D4 = 4 + 16 = 20
            " NOP \n"
            " NOP \n"
            " NOP \n"
            " NOP \n"
            " NOP \n"
            " NOP \n"
            " NOP \n"
            " NOP \n"
)

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {1, 2, 3, 4,16, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {1,20, 4,16,16, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
//*/

/* TEST CASE #3 READ AFTER WRITE HAZARD
DEFINE_TEST(Test1, 0,
            " MOV D2, #4\n"              // D2 = 4
            " MUL D3, D2, D3\n"         // D3 = D2 * D3 = 4 * 4 = 16
            " ADD D1, D2, D3\n"         // D1 = D2 + D4 = 4 + 16 = 20
)

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {1, 2, 3, 4,16, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {1,20, 4,16,16, 0, 0, 0, 0, 0}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
//*/

/* TEST CASE #2 REFERENCE INSTRUCTION
DEFINE_TEST(Test1, 0,
            " MUL D0, D1, D2\n"         // D0 = D1 * D2 = 2 * 3 = 6
)

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {6, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
//*/

/* TEST CASE #1 EMPTY
DEFINE_TEST(Test1, 0,
)

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };
//*/