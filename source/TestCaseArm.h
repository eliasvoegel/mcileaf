//* TEST CASE # UNALIGNED ACCESS
#define TEST_VAR_VALUE 0x1234567812345678
#define TEST_VAR_VALUE_SHIFTED 0x56781234
volatile uint64_t test_var0 = TEST_VAR_VALUE;

DEFINE_TEST(Test1, 0,
        " LDR R0, =test_var0\n"
        " LDR R1, [R0, #2] \n"
        " MOV R2, R1\n"
        )

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {(uint32_t)&test_var0, TEST_VAR_VALUE_SHIFTED, TEST_VAR_VALUE_SHIFTED, 0, 0, 0, 0, 0, 0, 0} };
//*/

/* TEST CASE #5 ALIGNED ACCESS
#define TEST_VAR_VALUE 0x1234567812345678
#define TEST_VAR_VALUE_NOT_SHIFTED 0x12345678
volatile uint64_t test_var0 = TEST_VAR_VALUE;

DEFINE_TEST(Test1, 0,
        " LDR R0, =test_var0\n"
        " LDR R1, [R0, #0] \n"
        " MOV R2, R1\n"
        )

// Register initial values for D0-D9 and A0-A9
RegisterContext_t test1_reg_initial     = { {0, 0, 0, 0, 0, 0, 0, 0, 0, 0} };

// Register expected result values for D0-D9 and A0-A9
RegisterContext_t test1_reg_expected    = { {(uint32_t)&test_var0, TEST_VAR_VALUE_NOT_SHIFTED, TEST_VAR_VALUE_NOT_SHIFTED, 0, 0, 0, 0, 0, 0, 0} };
//*/


/* TEST CASE #4 NO READ AFTER WRITE HAZARD
DEFINE_TEST(Test1, 0,
            " MOV R2, #4\n"             // R2 = 4
            " MUL R3, R2, R3\n"         // R3 = R2 * R3 = 4 * 4 = 16
            " ADD R1, R2, R4\n"         // R1 = R2 + R4 = 4 + 16 = 20
)

// Register initial values for R0-R9
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4,16, 0, 0, 0, 0, 0} };

// Register expected result values for R0-R9
RegisterContext_t test1_reg_expected = { .data_r_ = {1,20, 4,16,16, 0, 0, 0, 0, 0} };
//*/


/* TEST CASE #3 READ AFTER WRITE HAZARD
DEFINE_TEST(Test1, 0,
            " MOV R2, #4\n"             // R2 = 4
            " MUL R3, R2, R3\n"         // R3 = R2 * R3 = 4 * 4 = 16
            " ADD R1, R2, R3\n"         // R1 = R2 + R3 = 4 + 16 = 20
)

// Register initial values for R0-R9
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4,16, 0, 0, 0, 0, 0} };

// Register expected result values for R0-R9
RegisterContext_t test1_reg_expected = { .data_r_ = {1,20, 4,16,16, 0, 0, 0, 0, 0} };
//*/


/* TEST CASE #2 REFERENCE INSTRUCTION
DEFINE_TEST(Test1, 0,
            " MUL R0, R1, R2\n"         // R0 = R1 * R2 = 2 * 3 = 6
)

// Register initial values for R0-R9
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} };

// Register expected result values for R0-R9
RegisterContext_t test1_reg_expected = { .data_r_ = {6, 2, 3, 4, 5, 6, 7, 8, 9, 10} };
//*/


/* TEST CASE #1 EMPTY
DEFINE_TEST(Test1, 0,
)

// Register initial values for R0-R9
RegisterContext_t test1_reg_initial  = { .data_r_ = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} };

// Register expected result values for R0-R9
RegisterContext_t test1_reg_expected = { .data_r_ = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10} };
//*/