import math
from dataclasses import dataclass

import serial
import serial.tools.list_ports
import os
from pathlib import WindowsPath
import time
import shutil
import subprocess


# Test if serial port with specified name can be opened
def test_port(name):
    s = serial.Serial(name, baudrate=115200, timeout=2)
    try:
        if not s.isOpen():
            s.open()

        return s.isOpen(), s
    except:
        pass

    return False, s


# Open all available (ports that can be opened) ports and return list of port names
def open_ports():
    ports = serial.tools.list_ports.comports()
    print(len(ports), 'ports found')

    ret_ports = []

    for p in ports:
        print(f'Test port {p.name}')
        success, serial_port = test_port(p.name)
        if success:
            print(f"Successfully tested port {p.name}")
            ret_ports.append(serial_port)

    return ret_ports


# Close all ports in list
def close_ports(ports):
    for p in ports:
        p.close()


# Test result data class
@dataclass
class Result:
    def __init__(self, result, cycle_count, instr_count, m1_cnt=0, m2_cnt=0, m3_cnt=0):
        self.result = result 			# Test successful flag
        self.cycle_count = cycle_count		# Amount of cycles measured for test run
        self.instr_count = instr_count		# Amount of instructions mesaured for test run
        self.m1_cnt = m1_cnt			# Measured M1 CNT (only used for AURIX and RISC-V)
        self.m2_cnt = m2_cnt			# Measured M2 CNT (only used for AURIX and RISC-V)
        self.m3_cnt = m3_cnt			# Measured M3 CNT (only used for AURIX)


# String conversion of test result
    def __str__(self):
        return f"Result: {self.result}, Cycle Count: {self.cycle_count}, Instruction Count: {self.instr_count}, M1 " \
               f"Count: {self.m1_cnt}, M2 Count: {self.m2_cnt}, M3 Count: {self.m3_cnt}"


# Test board base class
# Provides interface for building the test case for the architecture and flashing the code onto the microcontroller
# Implements funcitonality for reading results from board via UART and analyzes them
class Board:
    def __init__(self, identifier: str, cwd_path: WindowsPath):
        self.results = list[Result]()
        self.len_results = 32
        self.identifier = identifier
        self.cwd_path = cwd_path

# Setup environment for building the test case. Must be called before building.
    def setup(self):
        pass

# Building the code for the board.
    def build(self) -> bool:
        pass

# Flash the code onto the board.
    def flash(self) -> bool:
        pass

# Search for the serial port that sends back the results. Returns this serial port or None, if not found.
    def getReadablePort(self, ports, timeout_ms: int) -> serial.Serial | None:
        for i in range(timeout_ms):
            for p in ports:
                if p.inWaiting():
                    id_len = len(self.identifier)
                    try:
                        s = str(p.read(id_len), 'UTF-8')

                        if s == self.identifier:
                            print(f"Connected to device {self.identifier}")
                            return p
                    except:
                        pass

                time.sleep(0.001)

        return None

# Read the result from the board via UART. Add result to the list of results of the board. 
    def readResult(self, ports) -> bool:
        print("Read results now.")
        timeout = 1000

        if self.identifier == "aurix":
            timeout = 10000

        port = self.getReadablePort(ports, timeout)

        if port is None:
            return False

        for i in range(self.len_results):
            read_length = 12
            if self.identifier == "aurix":
                read_length = 24
            elif self.identifier == "risc":
                read_length = 20

            b = port.read(read_length)
            result = int.from_bytes(b[0:4], "little", signed=True)
            cycle_count = int.from_bytes(b[4:8], "little", signed=True)
            instr_count = int.from_bytes(b[8:12], "little", signed=True)

            if self.identifier == "aurix" or self.identifier == "risc":
                m1_cnt = int.from_bytes(b[12:16], "little", signed=True)
                m2_cnt = int.from_bytes(b[16:20], "little", signed=True)
                m3_cnt = 0
                if self.identifier == "aurix":
                    m3_cnt = int.from_bytes(b[20:24], "little", signed=True)

                r = Result(result, cycle_count, instr_count, m1_cnt, m2_cnt, m3_cnt)
            else:
                r = Result(result, cycle_count, instr_count)

            # print(r)
            self.results.append(r)
        return True

# Analyze the received results. Calculate minimum and maximum values for each parameter. 
# Prints results to output terminal.
    def analyzeResult(self) -> bool:
        cycle_count_min = math.inf
        cycle_count_max = 0
        instr_count_min = math.inf
        instr_count_max = 0
        m1cnt_count_min = math.inf
        m1cnt_count_max = 0
        m2cnt_count_min = math.inf
        m2cnt_count_max = 0
        m3cnt_count_min = math.inf
        m3cnt_count_max = 0

        registers_correct = True

        for r in self.results:
            cycle_count_min = min(r.cycle_count, cycle_count_min)
            cycle_count_max = max(r.cycle_count, cycle_count_max)
            instr_count_min = min(r.instr_count, instr_count_min)
            instr_count_max = max(r.instr_count, instr_count_max)
            m1cnt_count_min = min(r.m1_cnt, m1cnt_count_min)
            m1cnt_count_max = max(r.m1_cnt, m1cnt_count_max)
            m2cnt_count_min = min(r.m2_cnt, m2cnt_count_min)
            m2cnt_count_max = max(r.m2_cnt, m2cnt_count_max)
            m3cnt_count_min = min(r.m3_cnt, m3cnt_count_min)
            m3cnt_count_max = max(r.m3_cnt, m3cnt_count_max)

            if r.result == 0:
                registers_correct = False

        print(f"Cycle count minimum: {cycle_count_min}")
        print(f"Cycle count maximum: {cycle_count_max}")
        print(f"Instr count minimum: {instr_count_min}")
        print(f"Instr count maximum: {instr_count_max}")
        print(f"M1 count minimum: {m1cnt_count_min}")
        print(f"M1 count maximum: {m1cnt_count_max}")
        print(f"M2 count minimum: {m2cnt_count_min}")
        print(f"M2 count maximum: {m2cnt_count_max}")
        print(f"M3 count minimum: {m3cnt_count_min}")
        print(f"M3 count maximum: {m3cnt_count_max}")

        print(f"Final register values are {'' if registers_correct else 'in'}correct")

        # Write result file
        path = cwd_path.joinpath(f"result_{self.identifier}.txt")
        f = open(path, "w")
        f.write(f"Result for {self.identifier}:\n")
        f.write(f"Cycle count minimum: {cycle_count_min}\n")
        f.write(f"Cycle count maximum: {cycle_count_max}\n")
        f.write(f"Instr count minimum: {instr_count_min}\n")
        f.write(f"Instr count maximum: {instr_count_max}\n")
        f.write(f"M1 count minimum: {m1cnt_count_min}\n")
        f.write(f"M1 count maximum: {m1cnt_count_max}\n")
        f.write(f"M2 count minimum: {m2cnt_count_min}\n")
        f.write(f"M2 count maximum: {m2cnt_count_max}\n")
        f.write(f"M3 count minimum: {m3cnt_count_min}\n")
        f.write(f"M3 count maximum: {m3cnt_count_max}\n")
        f.write(f"Final register values are {'' if registers_correct else 'in'}correct\n")

        f.write("\n\n\nSingle test results:\n")

        cnt = 0
        for r in self.results:
            f.write(f"Test run {cnt}: {r}\n")
            cnt = cnt + 1

        f.close()

        return True

# XMC4500 board implementation
class Xmc4500Board(Board):
    def __init__(self, source_path: WindowsPath, cwd_path: WindowsPath):
    # Set build paths
        super().__init__("arm", cwd_path)
        self.source_path = source_path
        self.build_path = source_path.joinpath("build", "cmake-build-debug-auto")
        print(self.build_path)
        self.len_results = 64

# Setup before build. Copies test case into project.
    def setup(self):
        dst = self.source_path.joinpath("inc", "TestCaseArm.h")
        src = cwd_path.joinpath("TestCaseArm.h")
        shutil.copyfile(src, dst)

# Build code project using cmake. 
    def build(self) -> bool:
        make_path = WindowsPath.cwd().joinpath("make.exe")
        arg_cmake_ = f"-DCMAKE_BUILD_TYPE=Debug -S {self.source_path} -B {self.build_path} -G \"MinGW Makefiles\" " \
                     f"-DCMAKE_MAKE_PROGRAM={make_path}"
        arg_cmake_build = f"--build {self.build_path} --target all -- -j 12"
        os.system(f'cmake {arg_cmake_}')
        os.system(f'cmake {arg_cmake_build}')
        print(make_path)
        return True

# Flash the code onto the microcontroller using SEGGER JLink
    def flash(self) -> bool:
        file = open("JLinkCommandFile.jlink", 'w')
        binary_path = os.path.join(self.build_path, "TestTarget.bin")
        file.write(f"USB 551020577\nloadbin {binary_path},0x08000000\nexit")
        file.close()

        jlink_arg = f"-device XMC4500-1024 -if SWD -speed auto -autoconnect 1 -NoGui 1 -CommanderScript " \
                    f"JLinkCommandFile.jlink"

        jlink_path = WindowsPath(os.path.join("c:", os.sep, "\"Program Files (x86)\"", "SEGGER", "JLink", "JLink.exe"))
        command = f"{jlink_path} {jlink_arg}"
        print(command)
        os.system(command)
        print("Finished flashing XMC4500.")
        return True

    def __str__(self):
        return f"XMC4500 (ARM Cortex M4)"

# RISC-V board implementation.
class RiscVBoard(Board):
    def __init__(self, source_path: WindowsPath, cwd_path: WindowsPath):
    # Set build paths
        super().__init__("risc", cwd_path)
        self.path = source_path
        self.build_path = source_path.joinpath("AssemblerTestProject")  # , "src")
        print(self.build_path)
        self.len_results = 32

# Setup before build. Copies test case into project.
    def setup(self):
        dst = self.build_path.joinpath("src", "TestCaseRisc.h")
        src = cwd_path.joinpath("TestCaseRisc.h")
        shutil.copyfile(src, dst)

# Bulid the code project using freedom studio.
    def build(self) -> bool:
        paths = {"C:\\FreedomStudio\\SiFive\\sdk-utilities-1.0.1-2020.12.1",
                 "C:\\FreedomStudio\\SiFive\\msys64-1.0.0-2020.08.1\\usr\\bin",
                 "C:\\FreedomStudio\\SiFive\\jlink-6.80.1-2020.06.03",
                 "C:\\FreedomStudio\\SiFive\\riscv64-unknown-elf-toolchain-10.2.0-2020.12.8\\bin",
                 "C:/FreedomStudio/jre/bin/server",
                 "C:/FreedomStudio/jre/bin",
                 "C:\\FreedomStudio"}

        for path in paths:
            os.environ["PATH"] += os.pathsep + path

        freedom_project_path = "/" + self.build_path.__str__().replace("\\", "/").replace(":", "")
        print(freedom_project_path)

        os.environ["BSP_DIR"] = freedom_project_path + "/bsp"
        os.environ["FREEDOM_E_SDK_VENV_PATH"] = freedom_project_path + "/venv"
        #os.environ["FREEDOM_SDK_TOOLS_PATH"] = "/C/FreedomStudio/SiFive/sdk-utilities-1.0.1-2020.12.1"
        os.environ["FREEDOM_STUDIO_GDB"] = "riscv64-unknown-elf-gdb.exe"
        #os.environ["FREEDOM_STUDIO_INSTALL_PATH"] = "/C/FreedomStudio"
        #os.environ["FREEDOM_STUDIO_MSYS_PATH"] = "/C/FreedomStudio/SiFive/msys64-1.0.0-2020.08.1/usr/bin"
        os.environ["FREEDOM_STUDIO_OPENOCD"] = "openocd.exe"
        #os.environ["FREEDOM_STUDIO_OPENOCD_PATH"] = "/C/FreedomStudio/SiFive/riscv-openocd-0.10.0-2020.12.1/bin"
        os.environ["FREEDOM_STUDIO_PROJECT_NAME"] = "AssemblerTestProject"
        os.environ["FREEDOM_STUDIO_PROJECT_PATH"] = freedom_project_path
        os.environ["FREEDOM_STUDIO_QEMU"] = "qemu-system-riscv32.exe"
        #os.environ["FREEDOM_STUDIO_QEMU_PATH"] = "/C/FreedomStudio/SiFive/riscv-qemu-5.1.0-2020.08.1/bin"
        #os.environ["FREEDOM_STUDIO_TOOLCHAIN_PATH"] = "/C/FreedomStudio/SiFive/riscv64-unknown-elf-toolchain-10.2.0-2020.12.8/bin"
        #os.environ["FREEDOM_STUDIO_TRACE_DECODER"] = "dqr.exe"
        #os.environ["FREEDOM_STUDIO_TRACE_DECODER_PATH"] = "/C/FreedomStudio/SiFive/trace-decoder-0.10.6-2021.04.0"
        os.environ["FSDEVENVSH"] = "dev_env.sh"
        os.environ["METAL_SOURCE_PATH"] = freedom_project_path + "/freedom-metal"
        #os.environ["RISCV_PATH"] = "/C/FreedomStudio/SiFive/riscv64-unknown-elf-toolchain-10.2.0-2020.12.8"
        os.environ["PWD"] = freedom_project_path

        os.chdir(self.build_path)
        print(os.getcwd())
        
        command = f"make all CONFIGURATION=debug"
        if os.system(command) != 0:
            print("Build failed.")
            return False
        print(os.getcwd())
        return True

# Flash the binary onto the microcontroller via SEGGER JLink.
    def flash(self) -> bool:
        os.chdir(self.build_path)
        file = open("JLinkCommandFile.jlink", 'w')
        binary_path = os.path.join(self.build_path, "src", "debug", "main.hex")
        file.write(f"LE\nUSB 979018120\nSI JTAG\nJTAGCONF -1 -1\nDEVICE FE310\nSPEED 1000\nAUTOCONNECT 1\nloadfile {binary_path}\nr\nh\nSetPC 0x20010000\ngo\nexit")
        file.close()

        jlink_arg = f"-NoGui 1 -CommanderScript JLinkCommandFile.jlink"

        jlink_path = WindowsPath(os.path.join("c:", os.sep, "\"Program Files (x86)\"", "SEGGER", "JLink", "JLink.exe"))
        command = f"{jlink_path} {jlink_arg}"
        print(command)
        os.system(command)
        print("Finished flashing RISCV.")
        return True

    def __str__(self):
        return f"HiFive (RISC-V)"

# Infineon AURIX board implementation.
class AurixBoard(Board):
    def __init__(self, source_path: WindowsPath, cwd_path: WindowsPath):
    # Set build paths
        super().__init__("aurix", cwd_path)
        self.process = None
        self.source_path = source_path
        self.build_path = source_path.joinpath("AssemblerTestProject", "Debug")
        print(self.build_path)

# Setup before build. Copies test case into project.
    def setup(self):
        dst = self.source_path.joinpath("AssemblerTestProject", "TestCaseAurix.h")
        src = cwd_path.joinpath("TestCaseAurix.h")
        shutil.copyfile(src, dst)

# Bulid the code project using AURIX studio.
    def build(self) -> bool:
        aurix_studio_path = WindowsPath("C:/Infineon/AURIX-Studio-1.6.0/AURIX-studio.exe")
        command = f"{aurix_studio_path} -data {self.source_path}"

        # Force rebuild by deleting Debug directory
        if os.path.exists(self.build_path):
            shutil.rmtree(self.build_path)

# User has to flash and build in the GUI, as it is not possible via command line
        print("\n\n\nBuild and Flash project now.")
        self.process = subprocess.Popen(command)

        return True

    def flash(self) -> bool:
        return True

    def readResult(self, ports) -> bool:
        b = super().readResult(ports)
        # Kill the AURIX Studio process
        if isinstance(self.process, subprocess.Popen):
            self.process.kill()
        return b

    def __str__(self):
        return f"Infineon Tricore (AURIX)"

# Setup, build, flash and read/analyze results from one single board.
def test_board(board: Board, ports) -> bool:
    board.setup()

    if not board.build():
        return False

    if not board.flash():
        return False

    if not board.readResult(ports):
        return False

    if not board.analyzeResult():
        return False
    return True


if __name__ == '__main__':
    serial_ports = open_ports()
    print(f"Found {len(serial_ports)} ports.")

    cwd_path = WindowsPath(WindowsPath.cwd())
    parent_path = cwd_path.parent
    xmc_path = cwd_path.joinpath("architectures").joinpath("arm-m4-data-hazard")
    risc_path = cwd_path.joinpath("architectures").joinpath("riscv-hifive-rev1b")
    aurix_path = cwd_path.joinpath("architectures").joinpath("aurix-tricore")
    xmc4500_board = Xmc4500Board(xmc_path, cwd_path)
    risc_v_board = RiscVBoard(risc_path, cwd_path)
    aurix_board = AurixBoard(aurix_path, cwd_path)

# Select boards to test:
    # boards = [xmc4500_board, risc_v_board, aurix_board] # run tests on all three boards
    # boards = [xmc4500_board, risc_v_board] 
    # boards = [aurix_board]
    # boards = [risc_v_board]
    boards = [risc_v_board]

    success = True
    for board in boards:
        success &= test_board(board, serial_ports)
        if not success:
            print(f"Board {board} failed.")

    close_ports(serial_ports)

